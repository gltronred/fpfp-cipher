{-# LANGUAGE BangPatterns #-}

module Main where

import Prelude hiding (lookup)

import Control.Arrow
import Control.Monad
import Data.Char
import Data.Foldable
import Data.Function
import qualified Data.List as L
import Data.List (genericLength,minimumBy,sortBy,group,sort,zip3,zip4)
import qualified Data.Map.Strict as M
import Data.Map.Strict (Map)
import Data.Maybe
import qualified Data.Sequence as Seq
import Data.Sequence (Seq,ViewL(..),(|>))
import System.IO
import System.Random

{- info
http://haskell98.blogspot.ru/2016/03/blog-post_18.html
http://www.statistica.ru/local-portals/data-mining/analiz-tekstov/
http://dict.ruslang.ru/freq.php?act=show&dic=freq_2letters&title=%D7%E0%F1%F2%EE%F2%ED%EE%F1%F2%FC%20%E4%E2%F3%E1%F3%EA%E2%E5%ED%ED%FB%F5%20%F1%EE%F7%E5%F2%E0%ED%E8%E9
http://www.cs.sjsu.edu/faculty/stamp/RUA/homophonic.pdf
-}


{- top freq
CT, HO, EH, ТО, НА, OB, НИ, PA, ВО, КО,

СТО, ЕНО, НОВ, ТОВ, ОВО, OBA 
-}

newtype Freq = Freq { unFreq :: Int } deriving (Eq,Ord,Read,Show)

data Letter = Subst !Char
            | Crypt !Int
            deriving (Eq,Ord)
instance Show Letter where
  show (Subst c) = [c, ' ']
  show (Crypt x) = if x>9
                   then show x
                   else '0':show x

by90 :: String -> String
by90 s | length s < 90 = s
       | otherwise     = take 90 s ++ '\n' : by90 (drop 90 s)

newtype Cipher = Cipher { unCipher :: [Letter] }
instance Show Cipher where
  show (Cipher ls) = by90 $ concatMap (\l -> ' ' : show l) ls

toKnown :: Letter -> Maybe Char
toKnown (Crypt _) = Nothing
toKnown (Subst ' ') = Just ' '
toKnown (Subst c) = Just c

pprint :: Cipher -> IO ()
pprint (Cipher ls) = putStrLn $ by90 $ mapMaybe toKnown ls

ic :: [Int] -> Double
ic ns = let
  n = fromIntegral $ sum ns
  c = genericLength ns
  in c * sum (map (\k -> fromIntegral $ k*(k-1)) ns) / (n*(n-1))

icTable :: Double
icTable = 0.06399771748902149

readTable :: IO [Int]
readTable = map read . concatMap (tail . words) . lines <$> readFile "table.txt"

newtype Table = Table { getTable :: Map Int (Maybe Char) } deriving (Eq,Show)

empty :: Table
empty = Table M.empty

singleton :: Int -> Maybe Char -> Table
singleton k v = Table $ M.singleton k v

lookup :: Int -> Table -> Maybe (Maybe Char)
lookup k (Table t) = M.lookup k t

insert :: Int -> Maybe Char -> Table -> Table
insert k v (Table t) = Table $ M.insert k v t

table = Table $ M.fromList [ (47, Just 'ъ'), ( 0, Just ' '), (63, Just ' ') ]


alphabet :: [Char]
alphabet = ' ':['а'..'я'] ++ ['А'..'Я']

randomTable :: IO Table
randomTable = randomTable' [] [0..99]
  where randomTable' acc [] = return $! Table $ M.fromList acc
        randomTable' acc (x:xs) = do
          use <- randomRIO (0,9) :: IO Int
          letter <- if use == 0
                    then return Nothing
                    else Just . (alphabet!!) <$> randomRIO (0, length alphabet-1)
          randomTable' ((x,letter):acc) xs

crossTable :: Table -> Table -> IO (Table, Table)
crossTable (Table !t1) (Table !t2) = do
  cross <- randomRIO (0,99)
  return $! (Table $ M.unionWithKey (\k a b -> if k < cross then a else b) t1 t2,
             Table $ M.unionWithKey (\k a b -> if k < cross then b else a) t1 t2)

genetic :: (Table -> Double) -> Int -> Int -> IO Table
genetic f size steps = replicateM size randomTable >>= genetic' steps
  where genetic' :: Int -> [Table] -> IO Table
        genetic' 0 population = return $ minimumBy (compare`on`f) population
        genetic' steps population = do
          let n = length population
              m = n`div`2
              sorted = sortBy (compare`on`f) population
              minPop = take m population
          newPop <- forM ((last minPop, head minPop) : zip minPop (tail minPop)) $ \(m1,m2) -> do
            (t1,t2) <- crossTable m1 m2
            return $ if f t1 < f t2
                     then t1
                     else t2
          putStrLn $ "Step " ++ show steps ++ " to finish: " ++ show (f $ minimumBy (compare`on`f) newPop)
          genetic' (steps-1) $! minPop ++ newPop


pattern :: String -> [Int] -> [Table]
pattern pat text = snd $
                   foldl' (\(window,acc) l -> (update window l, case matches pat window of
                                                  Nothing -> acc
                                                  Just t  -> t : acc))
                   (makeWindow $ take (length pat) text, [])
                   text
  where update :: Seq a -> a -> Seq a
        update s x = (case Seq.viewl s of
                         EmptyL  -> Seq.empty
                         _ :< tl -> tl) |> x
        matches :: String -> Seq Int -> Maybe Table
        matches pat str = (\(b,mt) -> if b then mt else Nothing) $
                          foldl' (\(b,mt) (p,s) -> case p of
                                     '?' -> (b,mt)
                                     '_' -> case mt of
                                       Nothing -> (b,Just $ singleton s Nothing)
                                       Just t  -> case lookup s t of
                                         Nothing -> (b,Just $ insert s Nothing t)
                                         Just Nothing -> (b,mt)
                                         Just (Just c') -> (False,Nothing)
                                     c   -> case mt of
                                       Nothing -> (b,Just $ singleton s (Just c))
                                       Just t  -> case lookup s t of
                                         Nothing -> (b,Just $ insert s (Just c) t)
                                         Just Nothing -> (False, Nothing)
                                         Just (Just c') -> (c==c',mt))
                          (True, Nothing) $
                          zip pat $ toList str
        makeWindow :: [Int] -> Seq Int
        makeWindow = Seq.fromList


countWord :: String -> Cipher -> Int
countWord w = count w . map (\c -> case c of
                                Crypt _ -> '#'
                                Subst s -> s) . unCipher
  where count :: String -> String -> Int
        count w s = length $ filter (\i -> take (length w) (drop i s) == w) [0..length s-1]

digramScore :: Map String Double -> Cipher -> Double
digramScore digram cipher = sum $ map (\((s1,s2),c) -> case M.lookup [toLower s1,toLower s2] digram of
                                          Nothing -> 0.05
                                          Just d  -> abs $ c-d) $ normTable $ map (second unFreq) $ freq $ tuple $ mapMaybe toKnown $ unCipher cipher

normTable :: [(a,Int)] -> [(a,Double)]
normTable dt = let
  s = fromIntegral $ sum $ map snd dt
  in map (second $ (/s) . fromIntegral) dt
  
digramsTable :: IO [(String,Int)]
digramsTable = map ((\ws -> (ws!!1,read $ ws!!2)) . words) . lines <$> readFile "digrams.txt"

type Forbidden = [Int]

swapLetters :: Forbidden -> Table -> IO Table
swapLetters forbidden t = do
  i <- randomRIO (0,99)
  j <- randomRIO (0,99)
  if i==j
    then return t
    else if i`elem`forbidden || j`elem`forbidden
         then swapLetters forbidden t
         else case (lookup i t, lookup j t) of
           (Just c1, Just c2) -> return $ insert j c1 $ insert i c2 t
           _ -> swapLetters forbidden t

changeLetters :: Forbidden -> Table -> IO Table
changeLetters forbidden t = do
  i <- randomRIO (0,99)
  if i`elem`forbidden
    then changeLetters forbidden t
    else case lookup i t of
      Nothing -> changeLetters forbidden t
      Just (Just c) -> return $ insert i Nothing t
      Just Nothing  -> do
        ci <- randomRIO (0,length alphabet-1)
        return $ insert i (Just $ alphabet !! ci) t

initialTable :: Table -> [Int] -> Table
initialTable init xs = Table $ flip M.union (getTable init) $ M.fromList $ zip (map fst $ freqCount id xs) (map Just alphabet ++ replicate 100 Nothing)

score dt cipher t = 1*(abs $ ic (map (unFreq . snd) $ freq $ map toKnown $ unCipher $ replaces t cipher) - icTable) + 100*digramScore dt (replaces t cipher)

climb :: Forbidden -> Map String Double -> Cipher -> Table -> (Forbidden -> Table -> IO Table) -> IO Table
climb forbidden dt cipher t f = do
  t' <- f forbidden t
  return $ if score dt cipher t' < score dt cipher t
           then t'
           else t

hillClimbing :: Int -> Table -> Maybe Table -> IO Table
hillClimbing k forbid mt = do
  !text <- work id
  !dt <- M.fromList . normTable <$> digramsTable
  let !table = fromMaybe (initialTable forbid text) mt
      !forbidden = M.keys $ getTable forbid
  hillClimbing' (toCipher text) forbidden dt table k

hillClimbing' cipher forbid dt !table 0 = return table
hillClimbing' cipher forbid dt !table !k = do
  table' <- climb forbid dt cipher table $ if k`mod`10 == 0 then changeLetters else swapLetters
  when (k`mod`100==0) $ putStrLn $ show k ++ ": " ++ show (score dt cipher table')
  hillClimbing' cipher forbid dt table' $ k-1

patterns w p text = let
  ps = pattern p text
  in sortBy (compare`on`(\p -> countWord w (replaces p $ toCipher text))) ps

equalSide :: (Eq a) => [a] -> [a]
equalSide = map head . filter ((>1) . length) . group

freq :: (Eq a, Ord a) => [a] -> [(a, Freq)]
freq = map (head &&& Freq . length) . group . sort

tuple s = zip s $ tail s

triple s = zip3 s (tail s) (drop 2 s)

quadruple s = zip4 s (tail s) (drop 2 s) (drop 3 s)

toCipher = Cipher . map Crypt

replaces :: Table -> Cipher -> Cipher
replaces (Table rs) (Cipher cs) = Cipher $! map (replace rs) cs
  where replace rs (Subst c) = Subst c
        replace rs (Crypt x) = case join $ M.lookup x rs of
          Nothing -> Crypt x
          Just c  -> Subst c

freqCount f = reverse . sortBy (compare`on`snd) . freq . f

solve :: [Int] -> Cipher
solve = replaces table . toCipher

work :: (Show b) => ([Int] -> b) -> IO b
work f = readFile "cipher.txt" >>= return . f . map read . concatMap words . lines

main :: IO ()
main = do
  hSetEncoding stdout utf8
  normTable <$> digramsTable >>= print






{-triples

((98,63,9),Freq 2),((98,63,65),Freq 2),((99,71,38),Freq 2),((99,79,96),Freq 2),((99,98,13),Freq 2),((13,51,2),Freq 3),((15,35,37),Freq 3),((54,71,37),Freq 3),((59,63,99),Freq 3),((81,63,84),Freq 3),((90,48,26),Freq 3),((96,35,88),Freq 3),((98,63,75),Freq 5)
-}

{-
pairFreq :: [((Int,Int),Freq)]
pairFreq = read "[((0,1),Freq 1),((0,7),Freq 1),((0,8),Freq 1),((0,10),Freq 1),((0,13),Freq 1),((0,15),Freq 1),((0,19),Freq 1),((0,25),Freq 1),((0,27),Freq 1),((0,32),Freq 1),((0,34),Freq 1),((0,36),Freq 1),((0,42),Freq 1),((0,45),Freq 1),((0,46),Freq 1),((0,48),Freq 1),((0,52),Freq 1),((0,54),Freq 1),((0,57),Freq 1),((0,59),Freq 1),((0,61),Freq 1),((0,65),Freq 1),((0,66),Freq 1),((0,69),Freq 1),((0,70),Freq 1),((0,72),Freq 1),((0,73),Freq 1),((0,74),Freq 1),((0,84),Freq 1),((0,90),Freq 1),((0,93),Freq 1),((0,99),Freq 1),((1,2),Freq 1),((1,5),Freq 1),((1,8),Freq 1),((1,19),Freq 1),((1,23),Freq 1),((1,33),Freq 1),((1,45),Freq 1),((1,51),Freq 1),((1,60),Freq 1),((1,66),Freq 1),((1,91),Freq 1),((1,94),Freq 1),((1,96),Freq 1),((2,1),Freq 1),((2,6),Freq 1),((2,17),Freq 1),((2,25),Freq 1),((2,29),Freq 1),((2,30),Freq 1),((2,31),Freq 1),((2,35),Freq 1),((2,36),Freq 1),((2,39),Freq 1),((2,42),Freq 1),((2,51),Freq 1),((2,52),Freq 1),((2,55),Freq 1),((2,57),Freq 1),((2,80),Freq 1),((2,86),Freq 1),((2,87),Freq 1),((2,98),Freq 1),((2,99),Freq 1),((3,0),Freq 1),((3,6),Freq 1),((3,8),Freq 1),((3,46),Freq 1),((3,56),Freq 1),((3,69),Freq 1),((3,70),Freq 1),((3,76),Freq 1),((3,77),Freq 1),((3,99),Freq 1),((4,9),Freq 1),((4,10),Freq 1),((4,17),Freq 1),((4,20),Freq 1),((4,23),Freq 1),((4,26),Freq 1),((4,28),Freq 1),((4,36),Freq 1),((4,39),Freq 1),((4,41),Freq 1),((4,42),Freq 1),((4,48),Freq 1),((4,57),Freq 1),((4,64),Freq 1),((4,71),Freq 1),((4,74),Freq 1),((4,75),Freq 1),((4,76),Freq 1),((4,93),Freq 1),((4,96),Freq 1),((5,6),Freq 1),((5,7),Freq 1),((5,9),Freq 1),((5,10),Freq 1),((5,12),Freq 1),((5,17),Freq 1),((5,29),Freq 1),((5,34),Freq 1),((5,35),Freq 1),((5,41),Freq 1),((5,42),Freq 1),((5,46),Freq 1),((5,56),Freq 1),((5,57),Freq 1),((5,60),Freq 1),((5,64),Freq 1),((5,74),Freq 1),((5,80),Freq 1),((5,81),Freq 1),((5,83),Freq 1),((5,90),Freq 1),((5,98),Freq 1),((6,8),Freq 1),((6,9),Freq 1),((6,14),Freq 1),((6,29),Freq 1),((6,30),Freq 1),((6,33),Freq 1),((6,41),Freq 1),((6,44),Freq 1),((6,61),Freq 1),((6,62),Freq 1),((6,78),Freq 1),((6,79),Freq 1),((6,88),Freq 1),((6,91),Freq 1),((6,92),Freq 1),((6,94),Freq 1),((6,96),Freq 1),((6,97),Freq 1),((7,1),Freq 1),((7,4),Freq 1),((7,6),Freq 1),((7,15),Freq 1),((7,32),Freq 1),((7,36),Freq 1),((7,44),Freq 1),((7,49),Freq 1),((7,55),Freq 1),((7,60),Freq 1),((7,61),Freq 1),((7,64),Freq 1),((7,66),Freq 1),((7,67),Freq 1),((7,68),Freq 1),((7,77),Freq 1),((7,79),Freq 1),((7,83),Freq 1),((7,85),Freq 1),((7,88),Freq 1),((7,90),Freq 1),((7,99),Freq 1),((8,0),Freq 1),((8,2),Freq 1),((8,20),Freq 1),((8,21),Freq 1),((8,22),Freq 1),((8,24),Freq 1),((8,32),Freq 1),((8,34),Freq 1),((8,39),Freq 1),((8,41),Freq 1),((8,46),Freq 1),((8,58),Freq 1),((8,60),Freq 1),((8,63),Freq 1),((8,67),Freq 1),((8,68),Freq 1),((8,71),Freq 1),((8,72),Freq 1),((8,74),Freq 1),((8,75),Freq 1),((8,78),Freq 1),((8,80),Freq 1),((8,86),Freq 1),((8,89),Freq 1),((9,0),Freq 1),((9,8),Freq 1),((9,20),Freq 1),((9,24),Freq 1),((9,40),Freq 1),((9,42),Freq 1),((9,53),Freq 1),((9,56),Freq 1),((9,62),Freq 1),((9,66),Freq 1),((9,74),Freq 1),((9,85),Freq 1),((9,88),Freq 1),((9,89),Freq 1),((9,92),Freq 1),((9,94),Freq 1),((9,97),Freq 1),((9,99),Freq 1),((10,2),Freq 1),((10,14),Freq 1),((10,16),Freq 1),((10,33),Freq 1),((10,34),Freq 1),((10,63),Freq 1),((10,70),Freq 1),((10,72),Freq 1),((10,77),Freq 1),((10,78),Freq 1),((10,79),Freq 1),((10,87),Freq 1),((10,95),Freq 1),((10,96),Freq 1),((12,14),Freq 1),((12,28),Freq 1),((12,33),Freq 1),((12,38),Freq 1),((12,46),Freq 1),((12,51),Freq 1),((12,52),Freq 1),((12,59),Freq 1),((12,61),Freq 1),((12,74),Freq 1),((12,78),Freq 1),((12,79),Freq 1),((12,82),Freq 1),((12,83),Freq 1),((12,99),Freq 1),((13,7),Freq 1),((13,9),Freq 1),((13,10),Freq 1),((13,17),Freq 1),((13,21),Freq 1),((13,25),Freq 1),((13,36),Freq 1),((13,45),Freq 1),((13,47),Freq 1),((13,50),Freq 1),((13,55),Freq 1),((13,59),Freq 1),((13,71),Freq 1),((13,76),Freq 1),((13,77),Freq 1),((13,93),Freq 1),((13,96),Freq 1),((14,6),Freq 1),((14,8),Freq 1),((14,9),Freq 1),((14,27),Freq 1),((14,28),Freq 1),((14,30),Freq 1),((14,50),Freq 1),((14,56),Freq 1),((14,60),Freq 1),((14,64),Freq 1),((14,69),Freq 1),((14,75),Freq 1),((14,76),Freq 1),((14,91),Freq 1),((14,94),Freq 1),((14,95),Freq 1),((14,96),Freq 1),((15,0),Freq 1),((15,10),Freq 1),((15,12),Freq 1),((15,21),Freq 1),((15,27),Freq 1),((15,29),Freq 1),((15,36),Freq 1),((15,39),Freq 1),((15,43),Freq 1),((15,61),Freq 1),((15,65),Freq 1),((15,69),Freq 1),((15,70),Freq 1),((15,71),Freq 1),((15,76),Freq 1),((15,77),Freq 1),((15,78),Freq 1),((15,80),Freq 1),((15,81),Freq 1),((15,87),Freq 1),((15,94),Freq 1),((15,98),Freq 1),((16,2),Freq 1),((16,5),Freq 1),((16,18),Freq 1),((16,22),Freq 1),((16,36),Freq 1),((16,48),Freq 1),((16,69),Freq 1),((16,95),Freq 1),((16,99),Freq 1),((17,1),Freq 1),((17,15),Freq 1),((17,19),Freq 1),((17,21),Freq 1),((17,22),Freq 1),((17,24),Freq 1),((17,27),Freq 1),((17,31),Freq 1),((17,33),Freq 1),((17,39),Freq 1),((17,48),Freq 1),((17,49),Freq 1),((17,54),Freq 1),((17,55),Freq 1),((17,56),Freq 1),((17,60),Freq 1),((17,67),Freq 1),((17,71),Freq 1),((17,72),Freq 1),((17,88),Freq 1),((17,91),Freq 1),((18,0),Freq 1),((18,7),Freq 1),((18,12),Freq 1),((18,20),Freq 1),((18,21),Freq 1),((18,25),Freq 1),((18,29),Freq 1),((18,30),Freq 1),((18,35),Freq 1),((18,42),Freq 1),((18,49),Freq 1),((18,52),Freq 1),((18,56),Freq 1),((18,57),Freq 1),((18,58),Freq 1),((18,60),Freq 1),((18,71),Freq 1),((18,74),Freq 1),((18,75),Freq 1),((18,80),Freq 1),((18,87),Freq 1),((18,91),Freq 1),((19,2),Freq 1),((19,5),Freq 1),((19,9),Freq 1),((19,10),Freq 1),((19,15),Freq 1),((19,32),Freq 1),((19,33),Freq 1),((19,38),Freq 1),((19,49),Freq 1),((19,66),Freq 1),((19,69),Freq 1),((19,74),Freq 1),((19,85),Freq 1),((19,89),Freq 1),((19,91),Freq 1),((19,95),Freq 1),((19,97),Freq 1),((20,0),Freq 1),((20,5),Freq 1),((20,9),Freq 1),((20,18),Freq 1),((20,29),Freq 1),((20,32),Freq 1),((20,33),Freq 1),((20,42),Freq 1),((20,61),Freq 1),((20,69),Freq 1),((20,72),Freq 1),((20,80),Freq 1),((20,82),Freq 1),((20,86),Freq 1),((21,8),Freq 1),((21,15),Freq 1),((21,17),Freq 1),((21,20),Freq 1),((21,22),Freq 1),((21,25),Freq 1),((21,30),Freq 1),((21,38),Freq 1),((21,42),Freq 1),((21,45),Freq 1),((21,57),Freq 1),((21,59),Freq 1),((21,60),Freq 1),((21,71),Freq 1),((21,74),Freq 1),((21,78),Freq 1),((21,79),Freq 1),((21,85),Freq 1),((21,94),Freq 1),((22,1),Freq 1),((22,7),Freq 1),((22,8),Freq 1),((22,12),Freq 1),((22,13),Freq 1),((22,29),Freq 1),((22,41),Freq 1),((22,56),Freq 1),((22,61),Freq 1),((22,71),Freq 1),((22,76),Freq 1),((22,81),Freq 1),((22,83),Freq 1),((22,99),Freq 1),((23,10),Freq 1),((23,28),Freq 1),((23,78),Freq 1),((23,81),Freq 1),((24,3),Freq 1),((24,21),Freq 1),((24,25),Freq 1),((24,30),Freq 1),((24,42),Freq 1),((24,46),Freq 1),((24,49),Freq 1),((24,51),Freq 1),((24,55),Freq 1),((24,58),Freq 1),((24,65),Freq 1),((24,69),Freq 1),((24,70),Freq 1),((24,83),Freq 1),((24,87),Freq 1),((24,90),Freq 1),((24,91),Freq 1),((24,93),Freq 1),((25,0),Freq 1),((25,2),Freq 1),((25,5),Freq 1),((25,10),Freq 1),((25,30),Freq 1),((25,33),Freq 1),((25,35),Freq 1),((25,37),Freq 1),((25,39),Freq 1),((25,44),Freq 1),((25,65),Freq 1),((25,78),Freq 1),((25,92),Freq 1),((26,8),Freq 1),((26,22),Freq 1),((26,40),Freq 1),((26,53),Freq 1),((26,79),Freq 1),((26,95),Freq 1),((26,99),Freq 1),((27,4),Freq 1),((27,9),Freq 1),((27,10),Freq 1),((27,14),Freq 1),((27,18),Freq 1),((27,22),Freq 1),((27,39),Freq 1),((27,48),Freq 1),((27,54),Freq 1),((27,70),Freq 1),((27,72),Freq 1),((27,79),Freq 1),((27,95),Freq 1),((28,6),Freq 1),((28,10),Freq 1),((28,31),Freq 1),((28,33),Freq 1),((28,35),Freq 1),((28,36),Freq 1),((28,39),Freq 1),((28,40),Freq 1),((28,44),Freq 1),((28,46),Freq 1),((28,48),Freq 1),((28,55),Freq 1),((28,60),Freq 1),((28,62),Freq 1),((28,66),Freq 1),((28,68),Freq 1),((28,72),Freq 1),((28,75),Freq 1),((28,78),Freq 1),((28,83),Freq 1),((28,86),Freq 1),((28,95),Freq 1),((29,7),Freq 1),((29,30),Freq 1),((29,41),Freq 1),((29,44),Freq 1),((29,49),Freq 1),((29,52),Freq 1),((29,59),Freq 1),((29,70),Freq 1),((29,75),Freq 1),((29,82),Freq 1),((29,88),Freq 1),((29,91),Freq 1),((30,5),Freq 1),((30,19),Freq 1),((30,35),Freq 1),((30,40),Freq 1),((30,44),Freq 1),((30,48),Freq 1),((30,55),Freq 1),((30,62),Freq 1),((30,69),Freq 1),((30,70),Freq 1),((30,72),Freq 1),((30,73),Freq 1),((30,80),Freq 1),((30,89),Freq 1),((31,0),Freq 1),((31,1),Freq 1),((31,6),Freq 1),((31,17),Freq 1),((31,21),Freq 1),((31,27),Freq 1),((31,28),Freq 1),((31,29),Freq 1),((31,35),Freq 1),((31,51),Freq 1),((31,56),Freq 1),((31,57),Freq 1),((31,58),Freq 1),((31,65),Freq 1),((31,67),Freq 1),((31,74),Freq 1),((31,75),Freq 1),((31,81),Freq 1),((31,85),Freq 1),((31,86),Freq 1),((31,87),Freq 1),((31,91),Freq 1),((31,93),Freq 1),((31,96),Freq 1),((32,6),Freq 1),((32,10),Freq 1),((32,12),Freq 1),((32,14),Freq 1),((32,19),Freq 1),((32,21),Freq 1),((32,26),Freq 1),((32,35),Freq 1),((32,50),Freq 1),((32,51),Freq 1),((32,56),Freq 1),((32,60),Freq 1),((32,65),Freq 1),((32,66),Freq 1),((32,67),Freq 1),((32,73),Freq 1),((32,75),Freq 1),((32,77),Freq 1),((32,80),Freq 1),((32,83),Freq 1),((32,84),Freq 1),((32,87),Freq 1),((32,94),Freq 1),((32,96),Freq 1),((33,1),Freq 1),((33,3),Freq 1),((33,7),Freq 1),((33,12),Freq 1),((33,19),Freq 1),((33,27),Freq 1),((33,28),Freq 1),((33,35),Freq 1),((33,42),Freq 1),((33,45),Freq 1),((33,54),Freq 1),((33,58),Freq 1),((33,60),Freq 1),((33,70),Freq 1),((33,71),Freq 1),((33,75),Freq 1),((33,83),Freq 1),((33,84),Freq 1),((33,90),Freq 1),((33,96),Freq 1),((33,98),Freq 1),((33,99),Freq 1),((34,0),Freq 1),((34,7),Freq 1),((34,9),Freq 1),((34,10),Freq 1),((34,16),Freq 1),((34,25),Freq 1),((34,26),Freq 1),((34,28),Freq 1),((34,29),Freq 1),((34,46),Freq 1),((34,51),Freq 1),((34,52),Freq 1),((34,53),Freq 1),((34,55),Freq 1),((34,57),Freq 1),((34,59),Freq 1),((34,70),Freq 1),((34,71),Freq 1),((34,72),Freq 1),((34,73),Freq 1),((34,76),Freq 1),((34,77),Freq 1),((34,81),Freq 1),((34,83),Freq 1),((34,84),Freq 1),((34,86),Freq 1),((34,89),Freq 1),((34,96),Freq 1),((35,1),Freq 1),((35,5),Freq 1),((35,13),Freq 1),((35,15),Freq 1),((35,16),Freq 1),((35,21),Freq 1),((35,28),Freq 1),((35,32),Freq 1),((35,33),Freq 1),((35,34),Freq 1),((35,38),Freq 1),((35,41),Freq 1),((35,44),Freq 1),((35,48),Freq 1),((35,56),Freq 1),((35,62),Freq 1),((35,63),Freq 1),((35,68),Freq 1),((35,69),Freq 1),((35,72),Freq 1),((35,78),Freq 1),((35,83),Freq 1),((35,86),Freq 1),((35,89),Freq 1),((35,92),Freq 1),((35,93),Freq 1),((35,95),Freq 1),((35,96),Freq 1),((36,4),Freq 1),((36,8),Freq 1),((36,14),Freq 1),((36,17),Freq 1),((36,27),Freq 1),((36,28),Freq 1),((36,31),Freq 1),((36,33),Freq 1),((36,39),Freq 1),((36,41),Freq 1),((36,42),Freq 1),((36,52),Freq 1),((36,56),Freq 1),((36,59),Freq 1),((36,65),Freq 1),((36,71),Freq 1),((36,75),Freq 1),((36,79),Freq 1),((36,90),Freq 1),((36,94),Freq 1),((37,0),Freq 1),((37,6),Freq 1),((37,25),Freq 1),((37,27),Freq 1),((37,29),Freq 1),((37,30),Freq 1),((37,78),Freq 1),((37,80),Freq 1),((38,0),Freq 1),((38,7),Freq 1),((38,9),Freq 1),((38,20),Freq 1),((38,26),Freq 1),((38,29),Freq 1),((38,30),Freq 1),((38,32),Freq 1),((38,36),Freq 1),((38,41),Freq 1),((38,46),Freq 1),((38,49),Freq 1),((38,52),Freq 1),((38,61),Freq 1),((38,69),Freq 1),((38,73),Freq 1),((38,75),Freq 1),((38,77),Freq 1),((38,78),Freq 1),((38,99),Freq 1),((39,0),Freq 1),((39,2),Freq 1),((39,3),Freq 1),((39,6),Freq 1),((39,15),Freq 1),((39,33),Freq 1),((39,42),Freq 1),((39,46),Freq 1),((39,48),Freq 1),((39,56),Freq 1),((39,62),Freq 1),((39,68),Freq 1),((39,70),Freq 1),((39,72),Freq 1),((39,85),Freq 1),((39,95),Freq 1),((39,97),Freq 1),((39,99),Freq 1),((40,0),Freq 1),((40,1),Freq 1),((40,6),Freq 1),((40,8),Freq 1),((40,9),Freq 1),((40,21),Freq 1),((40,30),Freq 1),((40,35),Freq 1),((40,42),Freq 1),((40,51),Freq 1),((40,60),Freq 1),((40,65),Freq 1),((40,67),Freq 1),((40,77),Freq 1),((40,94),Freq 1),((41,2),Freq 1),((41,14),Freq 1),((41,15),Freq 1),((41,32),Freq 1),((41,36),Freq 1),((41,38),Freq 1),((41,43),Freq 1),((41,58),Freq 1),((41,65),Freq 1),((41,78),Freq 1),((41,82),Freq 1),((41,85),Freq 1),((41,95),Freq 1),((41,99),Freq 1),((42,5),Freq 1),((42,6),Freq 1),((42,9),Freq 1),((42,18),Freq 1),((42,19),Freq 1),((42,24),Freq 1),((42,28),Freq 1),((42,30),Freq 1),((42,31),Freq 1),((42,33),Freq 1),((42,37),Freq 1),((42,48),Freq 1),((42,49),Freq 1),((42,51),Freq 1),((42,52),Freq 1),((42,53),Freq 1),((42,67),Freq 1),((42,72),Freq 1),((42,78),Freq 1),((42,86),Freq 1),((42,91),Freq 1),((42,92),Freq 1),((42,95),Freq 1),((43,0),Freq 1),((43,13),Freq 1),((43,15),Freq 1),((43,18),Freq 1),((43,19),Freq 1),((43,38),Freq 1),((43,39),Freq 1),((43,54),Freq 1),((43,56),Freq 1),((43,65),Freq 1),((43,68),Freq 1),((43,75),Freq 1),((43,79),Freq 1),((43,89),Freq 1),((43,92),Freq 1),((44,0),Freq 1),((44,7),Freq 1),((44,9),Freq 1),((44,20),Freq 1),((44,21),Freq 1),((44,25),Freq 1),((44,29),Freq 1),((44,39),Freq 1),((44,41),Freq 1),((44,46),Freq 1),((44,51),Freq 1),((44,55),Freq 1),((44,64),Freq 1),((44,65),Freq 1),((44,67),Freq 1),((44,70),Freq 1),((44,76),Freq 1),((44,78),Freq 1),((44,83),Freq 1),((44,94),Freq 1),((44,97),Freq 1),((44,98),Freq 1),((45,2),Freq 1),((45,8),Freq 1),((45,13),Freq 1),((45,24),Freq 1),((45,27),Freq 1),((45,28),Freq 1),((45,35),Freq 1),((45,37),Freq 1),((45,44),Freq 1),((45,45),Freq 1),((45,59),Freq 1),((45,60),Freq 1),((45,63),Freq 1),((45,74),Freq 1),((45,75),Freq 1),((45,78),Freq 1),((45,81),Freq 1),((45,83),Freq 1),((45,85),Freq 1),((45,90),Freq 1),((45,91),Freq 1),((45,93),Freq 1),((45,97),Freq 1),((46,4),Freq 1),((46,5),Freq 1),((46,9),Freq 1),((46,27),Freq 1),((46,28),Freq 1),((46,32),Freq 1),((46,40),Freq 1),((46,49),Freq 1),((46,51),Freq 1),((46,73),Freq 1),((46,74),Freq 1),((46,75),Freq 1),((46,78),Freq 1),((46,80),Freq 1),((46,91),Freq 1),((46,93),Freq 1),((46,94),Freq 1),((47,5),Freq 1),((47,14),Freq 1),((47,24),Freq 1),((48,10),Freq 1),((48,12),Freq 1),((48,16),Freq 1),((48,22),Freq 1),((48,27),Freq 1),((48,29),Freq 1),((48,34),Freq 1),((48,46),Freq 1),((48,51),Freq 1),((48,57),Freq 1),((48,59),Freq 1),((48,64),Freq 1),((48,70),Freq 1),((48,75),Freq 1),((48,83),Freq 1),((48,86),Freq 1),((48,98),Freq 1),((48,99),Freq 1),((49,3),Freq 1),((49,5),Freq 1),((49,16),Freq 1),((49,22),Freq 1),((49,58),Freq 1),((49,62),Freq 1),((49,63),Freq 1),((49,67),Freq 1),((49,68),Freq 1),((49,71),Freq 1),((49,85),Freq 1),((49,92),Freq 1),((49,95),Freq 1),((50,40),Freq 1),((50,92),Freq 1),((50,95),Freq 1),((51,0),Freq 1),((51,1),Freq 1),((51,6),Freq 1),((51,18),Freq 1),((51,22),Freq 1),((51,36),Freq 1),((51,38),Freq 1),((51,48),Freq 1),((51,53),Freq 1),((51,58),Freq 1),((51,62),Freq 1),((51,64),Freq 1),((51,76),Freq 1),((51,78),Freq 1),((51,79),Freq 1),((51,83),Freq 1),((51,85),Freq 1),((51,87),Freq 1),((51,89),Freq 1),((51,91),Freq 1),((51,92),Freq 1),((52,4),Freq 1),((52,14),Freq 1),((52,33),Freq 1),((52,34),Freq 1),((52,44),Freq 1),((52,48),Freq 1),((52,52),Freq 1),((52,54),Freq 1),((52,56),Freq 1),((52,69),Freq 1),((52,75),Freq 1),((52,79),Freq 1),((53,1),Freq 1),((53,6),Freq 1),((53,7),Freq 1),((53,8),Freq 1),((53,9),Freq 1),((53,16),Freq 1),((53,19),Freq 1),((53,21),Freq 1),((53,28),Freq 1),((53,29),Freq 1),((53,35),Freq 1),((53,36),Freq 1),((53,39),Freq 1),((53,42),Freq 1),((53,57),Freq 1),((53,65),Freq 1),((53,66),Freq 1),((53,69),Freq 1),((53,74),Freq 1),((53,75),Freq 1),((53,76),Freq 1),((53,81),Freq 1),((53,83),Freq 1),((53,84),Freq 1),((53,87),Freq 1),((53,92),Freq 1),((53,97),Freq 1),((53,98),Freq 1),((54,1),Freq 1),((54,6),Freq 1),((54,7),Freq 1),((54,8),Freq 1),((54,9),Freq 1),((54,12),Freq 1),((54,20),Freq 1),((54,35),Freq 1),((54,36),Freq 1),((54,42),Freq 1),((54,45),Freq 1),((54,46),Freq 1),((54,49),Freq 1),((54,58),Freq 1),((54,64),Freq 1),((54,65),Freq 1),((54,66),Freq 1),((54,70),Freq 1),((54,77),Freq 1),((54,83),Freq 1),((54,84),Freq 1),((54,94),Freq 1),((55,1),Freq 1),((55,2),Freq 1),((55,14),Freq 1),((55,18),Freq 1),((55,22),Freq 1),((55,24),Freq 1),((55,33),Freq 1),((55,34),Freq 1),((55,38),Freq 1),((55,44),Freq 1),((55,48),Freq 1),((55,62),Freq 1),((55,63),Freq 1),((55,73),Freq 1),((55,77),Freq 1),((55,85),Freq 1),((55,88),Freq 1),((55,91),Freq 1),((55,97),Freq 1),((56,2),Freq 1),((56,5),Freq 1),((56,8),Freq 1),((56,15),Freq 1),((56,26),Freq 1),((56,29),Freq 1),((56,30),Freq 1),((56,33),Freq 1),((56,35),Freq 1),((56,37),Freq 1),((56,39),Freq 1),((56,40),Freq 1),((56,49),Freq 1),((56,54),Freq 1),((56,55),Freq 1),((56,61),Freq 1),((56,64),Freq 1),((56,72),Freq 1),((56,77),Freq 1),((56,84),Freq 1),((56,88),Freq 1),((56,89),Freq 1),((56,91),Freq 1),((56,94),Freq 1),((56,96),Freq 1),((56,97),Freq 1),((57,2),Freq 1),((57,6),Freq 1),((57,14),Freq 1),((57,22),Freq 1),((57,29),Freq 1),((57,31),Freq 1),((57,33),Freq 1),((57,40),Freq 1),((57,44),Freq 1),((57,48),Freq 1),((57,53),Freq 1),((57,54),Freq 1),((57,65),Freq 1),((57,68),Freq 1),((57,78),Freq 1),((57,83),Freq 1),((57,87),Freq 1),((57,88),Freq 1),((57,92),Freq 1),((58,0),Freq 1),((58,1),Freq 1),((58,2),Freq 1),((58,13),Freq 1),((58,14),Freq 1),((58,24),Freq 1),((58,25),Freq 1),((58,31),Freq 1),((58,32),Freq 1),((58,34),Freq 1),((58,38),Freq 1),((58,44),Freq 1),((58,53),Freq 1),((58,54),Freq 1),((58,56),Freq 1),((58,64),Freq 1),((58,67),Freq 1),((58,68),Freq 1),((58,74),Freq 1),((58,79),Freq 1),((58,83),Freq 1),((58,87),Freq 1),((58,89),Freq 1),((58,97),Freq 1),((59,0),Freq 1),((59,5),Freq 1),((59,13),Freq 1),((59,21),Freq 1),((59,22),Freq 1),((59,27),Freq 1),((59,29),Freq 1),((59,46),Freq 1),((59,49),Freq 1),((59,67),Freq 1),((59,69),Freq 1),((59,77),Freq 1),((59,88),Freq 1),((59,95),Freq 1),((59,97),Freq 1),((59,99),Freq 1),((60,0),Freq 1),((60,9),Freq 1),((60,15),Freq 1),((60,17),Freq 1),((60,21),Freq 1),((60,34),Freq 1),((60,38),Freq 1),((60,44),Freq 1),((60,54),Freq 1),((60,56),Freq 1),((60,68),Freq 1),((60,70),Freq 1),((60,71),Freq 1),((60,72),Freq 1),((60,97),Freq 1),((61,4),Freq 1),((61,14),Freq 1),((61,22),Freq 1),((61,38),Freq 1),((61,40),Freq 1),((61,65),Freq 1),((61,66),Freq 1),((61,72),Freq 1),((61,85),Freq 1),((61,89),Freq 1),((61,92),Freq 1),((62,12),Freq 1),((62,13),Freq 1),((62,20),Freq 1),((62,30),Freq 1),((62,39),Freq 1),((62,43),Freq 1),((62,49),Freq 1),((62,52),Freq 1),((62,54),Freq 1),((62,60),Freq 1),((62,65),Freq 1),((62,66),Freq 1),((62,81),Freq 1),((62,83),Freq 1),((62,87),Freq 1),((62,91),Freq 1),((62,92),Freq 1),((62,96),Freq 1),((62,99),Freq 1),((63,0),Freq 1),((63,6),Freq 1),((63,12),Freq 1),((63,20),Freq 1),((63,32),Freq 1),((63,34),Freq 1),((63,36),Freq 1),((63,39),Freq 1),((63,60),Freq 1),((63,73),Freq 1),((63,74),Freq 1),((63,76),Freq 1),((63,78),Freq 1),((63,91),Freq 1),((63,93),Freq 1),((63,97),Freq 1),((64,4),Freq 1),((64,5),Freq 1),((64,14),Freq 1),((64,20),Freq 1),((64,23),Freq 1),((64,24),Freq 1),((64,28),Freq 1),((64,30),Freq 1),((64,42),Freq 1),((64,43),Freq 1),((64,53),Freq 1),((64,55),Freq 1),((64,58),Freq 1),((64,71),Freq 1),((64,73),Freq 1),((64,74),Freq 1),((64,75),Freq 1),((64,80),Freq 1),((64,81),Freq 1),((64,85),Freq 1),((64,89),Freq 1),((64,92),Freq 1),((65,0),Freq 1),((65,2),Freq 1),((65,3),Freq 1),((65,4),Freq 1),((65,6),Freq 1),((65,24),Freq 1),((65,26),Freq 1),((65,31),Freq 1),((65,32),Freq 1),((65,40),Freq 1),((65,53),Freq 1),((65,62),Freq 1),((65,71),Freq 1),((65,72),Freq 1),((65,74),Freq 1),((65,77),Freq 1),((65,78),Freq 1),((65,85),Freq 1),((65,88),Freq 1),((65,91),Freq 1),((65,92),Freq 1),((65,97),Freq 1),((65,99),Freq 1),((66,2),Freq 1),((66,4),Freq 1),((66,22),Freq 1),((66,24),Freq 1),((66,31),Freq 1),((66,34),Freq 1),((66,40),Freq 1),((66,53),Freq 1),((66,72),Freq 1),((66,73),Freq 1),((66,74),Freq 1),((66,78),Freq 1),((66,82),Freq 1),((66,86),Freq 1),((66,88),Freq 1),((66,95),Freq 1),((66,97),Freq 1),((67,0),Freq 1),((67,2),Freq 1),((67,16),Freq 1),((67,17),Freq 1),((67,20),Freq 1),((67,23),Freq 1),((67,24),Freq 1),((67,25),Freq 1),((67,29),Freq 1),((67,39),Freq 1),((67,40),Freq 1),((67,42),Freq 1),((67,45),Freq 1),((67,53),Freq 1),((67,56),Freq 1),((67,59),Freq 1),((67,65),Freq 1),((67,85),Freq 1),((67,89),Freq 1),((67,92),Freq 1),((67,94),Freq 1),((67,98),Freq 1),((68,7),Freq 1),((68,9),Freq 1),((68,10),Freq 1),((68,16),Freq 1),((68,29),Freq 1),((68,30),Freq 1),((68,36),Freq 1),((68,39),Freq 1),((68,41),Freq 1),((68,42),Freq 1),((68,43),Freq 1),((68,46),Freq 1),((68,49),Freq 1),((68,52),Freq 1),((68,58),Freq 1),((68,61),Freq 1),((68,65),Freq 1),((68,74),Freq 1),((68,80),Freq 1),((68,83),Freq 1),((68,97),Freq 1),((69,7),Freq 1),((69,10),Freq 1),((69,16),Freq 1),((69,20),Freq 1),((69,24),Freq 1),((69,27),Freq 1),((69,30),Freq 1),((69,35),Freq 1),((69,40),Freq 1),((69,42),Freq 1),((69,45),Freq 1),((69,53),Freq 1),((69,56),Freq 1),((69,65),Freq 1),((69,66),Freq 1),((69,71),Freq 1),((69,72),Freq 1),((69,73),Freq 1),((69,74),Freq 1),((69,78),Freq 1),((69,80),Freq 1),((69,91),Freq 1),((69,92),Freq 1),((69,94),Freq 1),((69,96),Freq 1),((70,0),Freq 1),((70,2),Freq 1),((70,23),Freq 1),((70,28),Freq 1),((70,32),Freq 1),((70,33),Freq 1),((70,39),Freq 1),((70,43),Freq 1),((70,44),Freq 1),((70,45),Freq 1),((70,49),Freq 1),((70,55),Freq 1),((70,57),Freq 1),((70,62),Freq 1),((70,65),Freq 1),((70,66),Freq 1),((70,74),Freq 1),((70,78),Freq 1),((70,79),Freq 1),((70,80),Freq 1),((70,81),Freq 1),((70,91),Freq 1),((71,1),Freq 1),((71,10),Freq 1),((71,14),Freq 1),((71,27),Freq 1),((71,34),Freq 1),((71,40),Freq 1),((71,44),Freq 1),((71,46),Freq 1),((71,58),Freq 1),((71,69),Freq 1),((71,70),Freq 1),((71,89),Freq 1),((71,95),Freq 1),((71,97),Freq 1),((71,99),Freq 1),((72,0),Freq 1),((72,7),Freq 1),((72,9),Freq 1),((72,10),Freq 1),((72,12),Freq 1),((72,14),Freq 1),((72,20),Freq 1),((72,26),Freq 1),((72,30),Freq 1),((72,36),Freq 1),((72,41),Freq 1),((72,43),Freq 1),((72,46),Freq 1),((72,57),Freq 1),((72,59),Freq 1),((72,65),Freq 1),((72,67),Freq 1),((72,73),Freq 1),((72,74),Freq 1),((72,76),Freq 1),((72,80),Freq 1),((72,81),Freq 1),((72,83),Freq 1),((72,84),Freq 1),((72,85),Freq 1),((72,90),Freq 1),((72,91),Freq 1),((72,93),Freq 1),((72,96),Freq 1),((72,98),Freq 1),((73,0),Freq 1),((73,14),Freq 1),((73,15),Freq 1),((73,18),Freq 1),((73,22),Freq 1),((73,27),Freq 1),((73,31),Freq 1),((73,34),Freq 1),((73,38),Freq 1),((73,41),Freq 1),((73,44),Freq 1),((73,60),Freq 1),((73,63),Freq 1),((73,66),Freq 1),((73,68),Freq 1),((73,71),Freq 1),((73,86),Freq 1),((74,1),Freq 1),((74,35),Freq 1),((74,36),Freq 1),((74,42),Freq 1),((74,49),Freq 1),((74,51),Freq 1),((74,53),Freq 1),((74,59),Freq 1),((74,64),Freq 1),((74,65),Freq 1),((74,87),Freq 1),((74,91),Freq 1),((75,3),Freq 1),((75,4),Freq 1),((75,13),Freq 1),((75,14),Freq 1),((75,21),Freq 1),((75,22),Freq 1),((75,26),Freq 1),((75,29),Freq 1),((75,33),Freq 1),((75,34),Freq 1),((75,36),Freq 1),((75,39),Freq 1),((75,40),Freq 1),((75,44),Freq 1),((75,53),Freq 1),((75,56),Freq 1),((75,62),Freq 1),((75,63),Freq 1),((75,64),Freq 1),((75,74),Freq 1),((75,77),Freq 1),((75,79),Freq 1),((75,82),Freq 1),((75,83),Freq 1),((75,86),Freq 1),((75,88),Freq 1),((75,92),Freq 1),((75,95),Freq 1),((75,97),Freq 1),((76,2),Freq 1),((76,27),Freq 1),((76,30),Freq 1),((76,41),Freq 1),((76,43),Freq 1),((76,45),Freq 1),((76,59),Freq 1),((76,62),Freq 1),((76,66),Freq 1),((76,73),Freq 1),((76,79),Freq 1),((76,89),Freq 1),((76,90),Freq 1),((76,94),Freq 1),((76,98),Freq 1),((77,2),Freq 1),((77,4),Freq 1),((77,5),Freq 1),((77,8),Freq 1),((77,10),Freq 1),((77,12),Freq 1),((77,13),Freq 1),((77,15),Freq 1),((77,17),Freq 1),((77,22),Freq 1),((77,26),Freq 1),((77,30),Freq 1),((77,35),Freq 1),((77,39),Freq 1),((77,40),Freq 1),((77,48),Freq 1),((77,52),Freq 1),((77,54),Freq 1),((77,57),Freq 1),((77,58),Freq 1),((77,62),Freq 1),((77,65),Freq 1),((77,71),Freq 1),((77,73),Freq 1),((77,74),Freq 1),((77,81),Freq 1),((77,92),Freq 1),((77,93),Freq 1),((77,94),Freq 1),((77,96),Freq 1),((78,1),Freq 1),((78,2),Freq 1),((78,9),Freq 1),((78,15),Freq 1),((78,18),Freq 1),((78,19),Freq 1),((78,24),Freq 1),((78,30),Freq 1),((78,31),Freq 1),((78,32),Freq 1),((78,35),Freq 1),((78,36),Freq 1),((78,39),Freq 1),((78,41),Freq 1),((78,42),Freq 1),((78,43),Freq 1),((78,46),Freq 1),((78,66),Freq 1),((78,69),Freq 1),((78,82),Freq 1),((78,85),Freq 1),((78,90),Freq 1),((78,91),Freq 1),((78,93),Freq 1),((78,94),Freq 1),((79,27),Freq 1),((79,29),Freq 1),((79,30),Freq 1),((79,35),Freq 1),((79,36),Freq 1),((79,41),Freq 1),((79,43),Freq 1),((79,46),Freq 1),((79,49),Freq 1),((79,55),Freq 1),((79,58),Freq 1),((79,59),Freq 1),((79,64),Freq 1),((79,71),Freq 1),((79,73),Freq 1),((79,75),Freq 1),((79,76),Freq 1),((79,78),Freq 1),((79,87),Freq 1),((79,94),Freq 1),((80,0),Freq 1),((80,5),Freq 1),((80,7),Freq 1),((80,13),Freq 1),((80,18),Freq 1),((80,19),Freq 1),((80,23),Freq 1),((80,36),Freq 1),((80,37),Freq 1),((80,39),Freq 1),((80,43),Freq 1),((80,48),Freq 1),((80,52),Freq 1),((80,53),Freq 1),((80,54),Freq 1),((80,56),Freq 1),((80,61),Freq 1),((80,64),Freq 1),((80,72),Freq 1),((80,74),Freq 1),((80,75),Freq 1),((80,82),Freq 1),((81,2),Freq 1),((81,10),Freq 1),((81,13),Freq 1),((81,15),Freq 1),((81,18),Freq 1),((81,21),Freq 1),((81,24),Freq 1),((81,31),Freq 1),((81,34),Freq 1),((81,35),Freq 1),((81,37),Freq 1),((81,48),Freq 1),((81,53),Freq 1),((81,62),Freq 1),((81,68),Freq 1),((81,73),Freq 1),((81,74),Freq 1),((81,82),Freq 1),((81,85),Freq 1),((81,88),Freq 1),((81,96),Freq 1),((81,97),Freq 1),((82,19),Freq 1),((82,30),Freq 1),((82,39),Freq 1),((82,55),Freq 1),((82,57),Freq 1),((82,66),Freq 1),((82,69),Freq 1),((82,71),Freq 1),((82,73),Freq 1),((82,77),Freq 1),((82,83),Freq 1),((83,7),Freq 1),((83,8),Freq 1),((83,10),Freq 1),((83,17),Freq 1),((83,28),Freq 1),((83,33),Freq 1),((83,38),Freq 1),((83,39),Freq 1),((83,41),Freq 1),((83,45),Freq 1),((83,47),Freq 1),((83,48),Freq 1),((83,51),Freq 1),((83,57),Freq 1),((83,60),Freq 1),((83,71),Freq 1),((83,73),Freq 1),((83,75),Freq 1),((83,80),Freq 1),((83,89),Freq 1),((83,91),Freq 1),((83,93),Freq 1),((83,94),Freq 1),((84,36),Freq 1),((84,46),Freq 1),((84,56),Freq 1),((84,67),Freq 1),((84,74),Freq 1),((84,94),Freq 1),((85,1),Freq 1),((85,6),Freq 1),((85,10),Freq 1),((85,16),Freq 1),((85,17),Freq 1),((85,19),Freq 1),((85,35),Freq 1),((85,36),Freq 1),((85,46),Freq 1),((85,51),Freq 1),((85,55),Freq 1),((85,60),Freq 1),((85,61),Freq 1),((85,66),Freq 1),((85,75),Freq 1),((85,78),Freq 1),((85,79),Freq 1),((85,82),Freq 1),((85,83),Freq 1),((85,84),Freq 1),((85,89),Freq 1),((85,93),Freq 1),((86,2),Freq 1),((86,5),Freq 1),((86,7),Freq 1),((86,9),Freq 1),((86,19),Freq 1),((86,35),Freq 1),((86,40),Freq 1),((86,42),Freq 1),((86,48),Freq 1),((86,49),Freq 1),((86,51),Freq 1),((86,56),Freq 1),((86,58),Freq 1),((86,60),Freq 1),((86,72),Freq 1),((86,79),Freq 1),((86,89),Freq 1),((86,91),Freq 1),((86,92),Freq 1),((86,94),Freq 1),((86,95),Freq 1),((87,9),Freq 1),((87,10),Freq 1),((87,17),Freq 1),((87,20),Freq 1),((87,27),Freq 1),((87,40),Freq 1),((87,41),Freq 1),((87,42),Freq 1),((87,49),Freq 1),((87,54),Freq 1),((87,56),Freq 1),((87,60),Freq 1),((87,61),Freq 1),((87,65),Freq 1),((87,66),Freq 1),((87,79),Freq 1),((87,81),Freq 1),((87,89),Freq 1),((87,90),Freq 1),((87,91),Freq 1),((87,93),Freq 1),((87,94),Freq 1),((87,96),Freq 1),((87,98),Freq 1),((88,0),Freq 1),((88,1),Freq 1),((88,7),Freq 1),((88,10),Freq 1),((88,19),Freq 1),((88,21),Freq 1),((88,23),Freq 1),((88,29),Freq 1),((88,35),Freq 1),((88,36),Freq 1),((88,39),Freq 1),((88,42),Freq 1),((88,49),Freq 1),((88,55),Freq 1),((88,59),Freq 1),((88,66),Freq 1),((88,70),Freq 1),((88,71),Freq 1),((88,74),Freq 1),((88,77),Freq 1),((88,80),Freq 1),((88,82),Freq 1),((88,95),Freq 1),((89,7),Freq 1),((89,30),Freq 1),((89,39),Freq 1),((89,42),Freq 1),((89,45),Freq 1),((89,51),Freq 1),((89,56),Freq 1),((89,57),Freq 1),((89,58),Freq 1),((89,59),Freq 1),((89,64),Freq 1),((89,67),Freq 1),((89,69),Freq 1),((89,73),Freq 1),((89,74),Freq 1),((89,80),Freq 1),((89,87),Freq 1),((89,94),Freq 1),((89,98),Freq 1),((89,99),Freq 1),((90,7),Freq 1),((90,15),Freq 1),((90,24),Freq 1),((90,28),Freq 1),((90,33),Freq 1),((90,34),Freq 1),((90,38),Freq 1),((90,43),Freq 1),((90,51),Freq 1),((90,53),Freq 1),((90,54),Freq 1),((90,56),Freq 1),((90,58),Freq 1),((90,63),Freq 1),((90,65),Freq 1),((90,68),Freq 1),((90,71),Freq 1),((90,79),Freq 1),((90,81),Freq 1),((90,82),Freq 1),((90,83),Freq 1),((90,92),Freq 1),((90,94),Freq 1),((90,95),Freq 1),((91,9),Freq 1),((91,13),Freq 1),((91,14),Freq 1),((91,22),Freq 1),((91,31),Freq 1),((91,32),Freq 1),((91,33),Freq 1),((91,34),Freq 1),((91,42),Freq 1),((91,44),Freq 1),((91,46),Freq 1),((91,48),Freq 1),((91,53),Freq 1),((91,56),Freq 1),((91,68),Freq 1),((91,69),Freq 1),((91,72),Freq 1),((91,74),Freq 1),((91,78),Freq 1),((91,85),Freq 1),((91,88),Freq 1),((91,92),Freq 1),((91,99),Freq 1),((92,8),Freq 1),((92,9),Freq 1),((92,13),Freq 1),((92,17),Freq 1),((92,30),Freq 1),((92,36),Freq 1),((92,42),Freq 1),((92,51),Freq 1),((92,64),Freq 1),((92,66),Freq 1),((92,67),Freq 1),((92,71),Freq 1),((92,73),Freq 1),((92,75),Freq 1),((92,80),Freq 1),((92,83),Freq 1),((92,84),Freq 1),((92,85),Freq 1),((92,86),Freq 1),((92,94),Freq 1),((92,95),Freq 1),((92,96),Freq 1),((93,5),Freq 1),((93,15),Freq 1),((93,18),Freq 1),((93,22),Freq 1),((93,27),Freq 1),((93,30),Freq 1),((93,39),Freq 1),((93,45),Freq 1),((93,53),Freq 1),((93,62),Freq 1),((93,64),Freq 1),((93,66),Freq 1),((93,68),Freq 1),((93,69),Freq 1),((93,73),Freq 1),((93,74),Freq 1),((93,79),Freq 1),((93,80),Freq 1),((93,83),Freq 1),((93,91),Freq 1),((93,94),Freq 1),((93,99),Freq 1),((94,18),Freq 1),((94,19),Freq 1),((94,20),Freq 1),((94,33),Freq 1),((94,37),Freq 1),((94,38),Freq 1),((94,39),Freq 1),((94,41),Freq 1),((94,54),Freq 1),((94,66),Freq 1),((94,71),Freq 1),((94,81),Freq 1),((94,82),Freq 1),((94,86),Freq 1),((94,90),Freq 1),((95,0),Freq 1),((95,8),Freq 1),((95,25),Freq 1),((95,30),Freq 1),((95,35),Freq 1),((95,39),Freq 1),((95,46),Freq 1),((95,51),Freq 1),((95,53),Freq 1),((95,70),Freq 1),((95,81),Freq 1),((95,87),Freq 1),((95,94),Freq 1),((95,97),Freq 1),((96,0),Freq 1),((96,27),Freq 1),((96,28),Freq 1),((96,31),Freq 1),((96,56),Freq 1),((96,69),Freq 1),((96,76),Freq 1),((96,78),Freq 1),((96,81),Freq 1),((96,85),Freq 1),((97,0),Freq 1),((97,4),Freq 1),((97,9),Freq 1),((97,10),Freq 1),((97,12),Freq 1),((97,16),Freq 1),((97,26),Freq 1),((97,28),Freq 1),((97,45),Freq 1),((97,46),Freq 1),((97,49),Freq 1),((97,69),Freq 1),((97,71),Freq 1),((97,73),Freq 1),((97,74),Freq 1),((97,78),Freq 1),((97,80),Freq 1),((97,81),Freq 1),((97,85),Freq 1),((97,91),Freq 1),((98,0),Freq 1),((98,4),Freq 1),((98,7),Freq 1),((98,15),Freq 1),((98,28),Freq 1),((98,33),Freq 1),((98,39),Freq 1),((98,42),Freq 1),((98,44),Freq 1),((98,45),Freq 1),((98,54),Freq 1),((98,56),Freq 1),((98,65),Freq 1),((98,66),Freq 1),((98,89),Freq 1),((99,5),Freq 1),((99,8),Freq 1),((99,10),Freq 1),((99,17),Freq 1),((99,23),Freq 1),((99,24),Freq 1),((99,26),Freq 1),((99,28),Freq 1),((99,30),Freq 1),((99,31),Freq 1),((99,38),Freq 1),((99,41),Freq 1),((99,42),Freq 1),((99,47),Freq 1),((99,52),Freq 1),((99,54),Freq 1),((99,56),Freq 1),((99,62),Freq 1),((99,72),Freq 1),((99,75),Freq 1),((99,80),Freq 1),((99,81),Freq 1),((99,89),Freq 1),((99,90),Freq 1),((99,91),Freq 1),((99,94),Freq 1),((99,96),Freq 1),((0,33),Freq 2),((0,40),Freq 2),((0,94),Freq 2),((0,97),Freq 2),((0,98),Freq 2),((1,28),Freq 2),((1,41),Freq 2),((1,43),Freq 2),((1,59),Freq 2),((1,71),Freq 2),((1,93),Freq 2),((2,8),Freq 2),((2,26),Freq 2),((2,59),Freq 2),((2,70),Freq 2),((2,83),Freq 2),((2,94),Freq 2),((3,21),Freq 2),((3,50),Freq 2),((3,64),Freq 2),((4,0),Freq 2),((4,8),Freq 2),((4,67),Freq 2),((4,77),Freq 2),((4,98),Freq 2),((5,21),Freq 2),((5,28),Freq 2),((5,30),Freq 2),((5,36),Freq 2),((5,65),Freq 2),((5,69),Freq 2),((5,77),Freq 2),((5,78),Freq 2),((5,86),Freq 2),((6,27),Freq 2),((6,28),Freq 2),((6,43),Freq 2),((6,49),Freq 2),((6,98),Freq 2),((7,0),Freq 2),((7,70),Freq 2),((7,87),Freq 2),((7,89),Freq 2),((8,13),Freq 2),((8,14),Freq 2),((8,18),Freq 2),((8,31),Freq 2),((8,37),Freq 2),((8,40),Freq 2),((8,62),Freq 2),((8,65),Freq 2),((8,79),Freq 2),((8,81),Freq 2),((9,13),Freq 2),((9,15),Freq 2),((9,19),Freq 2),((9,22),Freq 2),((9,38),Freq 2),((9,41),Freq 2),((9,44),Freq 2),((9,72),Freq 2),((10,13),Freq 2),((10,15),Freq 2),((10,19),Freq 2),((10,24),Freq 2),((10,32),Freq 2),((10,44),Freq 2),((10,66),Freq 2),((10,89),Freq 2),((10,92),Freq 2),((10,97),Freq 2),((12,5),Freq 2),((12,27),Freq 2),((12,73),Freq 2),((13,0),Freq 2),((13,14),Freq 2),((13,26),Freq 2),((13,35),Freq 2),((13,39),Freq 2),((13,60),Freq 2),((13,70),Freq 2),((13,78),Freq 2),((13,84),Freq 2),((13,99),Freq 2),((14,12),Freq 2),((14,21),Freq 2),((14,58),Freq 2),((14,59),Freq 2),((14,77),Freq 2),((14,93),Freq 2),((15,7),Freq 2),((15,8),Freq 2),((15,49),Freq 2),((15,59),Freq 2),((15,75),Freq 2),((15,83),Freq 2),((15,90),Freq 2),((15,96),Freq 2),((16,32),Freq 2),((17,44),Freq 2),((17,66),Freq 2),((17,77),Freq 2),((17,92),Freq 2),((18,10),Freq 2),((18,19),Freq 2),((18,51),Freq 2),((18,59),Freq 2),((18,77),Freq 2),((18,81),Freq 2),((18,90),Freq 2),((18,96),Freq 2),((19,4),Freq 2),((19,31),Freq 2),((19,40),Freq 2),((19,44),Freq 2),((19,54),Freq 2),((19,63),Freq 2),((19,78),Freq 2),((19,82),Freq 2),((20,15),Freq 2),((20,28),Freq 2),((20,48),Freq 2),((20,70),Freq 2),((20,95),Freq 2),((20,97),Freq 2),((21,5),Freq 2),((21,23),Freq 2),((21,28),Freq 2),((21,43),Freq 2),((21,81),Freq 2),((21,91),Freq 2),((21,92),Freq 2),((21,93),Freq 2),((21,98),Freq 2),((22,10),Freq 2),((22,26),Freq 2),((22,35),Freq 2),((22,36),Freq 2),((22,73),Freq 2),((22,80),Freq 2),((22,94),Freq 2),((22,96),Freq 2),((23,57),Freq 2),((23,73),Freq 2),((24,0),Freq 2),((24,9),Freq 2),((24,20),Freq 2),((24,35),Freq 2),((24,36),Freq 2),((24,39),Freq 2),((24,52),Freq 2),((24,56),Freq 2),((25,9),Freq 2),((25,13),Freq 2),((25,32),Freq 2),((25,53),Freq 2),((25,82),Freq 2),((25,95),Freq 2),((25,97),Freq 2),((26,13),Freq 2),((26,32),Freq 2),((26,34),Freq 2),((26,48),Freq 2),((26,81),Freq 2),((26,92),Freq 2),((26,94),Freq 2),((27,24),Freq 2),((27,31),Freq 2),((27,38),Freq 2),((27,62),Freq 2),((27,87),Freq 2),((27,92),Freq 2),((28,0),Freq 2),((28,5),Freq 2),((28,14),Freq 2),((28,15),Freq 2),((28,18),Freq 2),((28,22),Freq 2),((28,24),Freq 2),((28,53),Freq 2),((28,76),Freq 2),((28,77),Freq 2),((28,79),Freq 2),((28,92),Freq 2),((29,31),Freq 2),((29,43),Freq 2),((29,48),Freq 2),((29,67),Freq 2),((29,68),Freq 2),((29,73),Freq 2),((29,78),Freq 2),((30,4),Freq 2),((30,18),Freq 2),((30,24),Freq 2),((30,31),Freq 2),((30,36),Freq 2),((30,68),Freq 2),((30,79),Freq 2),((31,19),Freq 2),((31,43),Freq 2),((31,46),Freq 2),((31,59),Freq 2),((31,64),Freq 2),((31,83),Freq 2),((31,98),Freq 2),((32,8),Freq 2),((32,17),Freq 2),((32,45),Freq 2),((32,46),Freq 2),((32,55),Freq 2),((32,62),Freq 2),((32,69),Freq 2),((32,71),Freq 2),((33,8),Freq 2),((33,36),Freq 2),((33,39),Freq 2),((33,59),Freq 2),((33,73),Freq 2),((33,74),Freq 2),((33,81),Freq 2),((33,87),Freq 2),((34,39),Freq 2),((34,60),Freq 2),((34,66),Freq 2),((34,80),Freq 2),((35,18),Freq 2),((35,19),Freq 2),((35,20),Freq 2),((35,24),Freq 2),((35,30),Freq 2),((35,31),Freq 2),((35,40),Freq 2),((35,66),Freq 2),((35,79),Freq 2),((35,85),Freq 2),((35,97),Freq 2),((36,29),Freq 2),((36,40),Freq 2),((36,43),Freq 2),((36,92),Freq 2),((36,98),Freq 2),((37,1),Freq 2),((37,3),Freq 2),((37,42),Freq 2),((37,45),Freq 2),((37,58),Freq 2),((37,71),Freq 2),((38,6),Freq 2),((38,8),Freq 2),((38,35),Freq 2),((38,39),Freq 2),((38,42),Freq 2),((38,57),Freq 2),((38,65),Freq 2),((38,70),Freq 2),((38,76),Freq 2),((38,81),Freq 2),((39,18),Freq 2),((39,22),Freq 2),((39,40),Freq 2),((39,53),Freq 2),((39,67),Freq 2),((39,74),Freq 2),((39,78),Freq 2),((39,83),Freq 2),((39,92),Freq 2),((40,25),Freq 2),((40,59),Freq 2),((40,64),Freq 2),((40,80),Freq 2),((40,87),Freq 2),((41,0),Freq 2),((41,1),Freq 2),((41,24),Freq 2),((41,48),Freq 2),((41,54),Freq 2),((41,77),Freq 2),((41,83),Freq 2),((41,88),Freq 2),((41,97),Freq 2),((42,27),Freq 2),((42,43),Freq 2),((42,97),Freq 2),((43,14),Freq 2),((43,31),Freq 2),((43,34),Freq 2),((43,55),Freq 2),((43,88),Freq 2),((44,17),Freq 2),((44,42),Freq 2),((44,60),Freq 2),((44,69),Freq 2),((44,77),Freq 2),((44,91),Freq 2),((44,93),Freq 2),((45,18),Freq 2),((45,30),Freq 2),((45,31),Freq 2),((45,62),Freq 2),((45,72),Freq 2),((45,87),Freq 2),((45,88),Freq 2),((46,8),Freq 2),((46,19),Freq 2),((46,43),Freq 2),((46,58),Freq 2),((46,62),Freq 2),((48,21),Freq 2),((48,28),Freq 2),((48,36),Freq 2),((48,65),Freq 2),((48,73),Freq 2),((49,18),Freq 2),((49,24),Freq 2),((49,33),Freq 2),((49,38),Freq 2),((49,53),Freq 2),((49,72),Freq 2),((49,74),Freq 2),((49,97),Freq 2),((50,33),Freq 2),((51,24),Freq 2),((51,32),Freq 2),((51,33),Freq 2),((51,40),Freq 2),((51,56),Freq 2),((51,72),Freq 2),((51,88),Freq 2),((51,97),Freq 2),((51,99),Freq 2),((52,15),Freq 2),((52,19),Freq 2),((52,24),Freq 2),((52,88),Freq 2),((53,46),Freq 2),((53,51),Freq 2),((54,0),Freq 2),((54,17),Freq 2),((54,19),Freq 2),((54,25),Freq 2),((54,50),Freq 2),((54,51),Freq 2),((54,55),Freq 2),((54,59),Freq 2),((55,5),Freq 2),((55,15),Freq 2),((55,31),Freq 2),((55,40),Freq 2),((55,74),Freq 2),((55,78),Freq 2),((55,89),Freq 2),((56,3),Freq 2),((56,14),Freq 2),((56,45),Freq 2),((56,48),Freq 2),((56,62),Freq 2),((56,63),Freq 2),((57,36),Freq 2),((57,46),Freq 2),((57,64),Freq 2),((57,67),Freq 2),((58,72),Freq 2),((58,88),Freq 2),((59,1),Freq 2),((59,2),Freq 2),((59,14),Freq 2),((59,15),Freq 2),((59,31),Freq 2),((59,33),Freq 2),((59,34),Freq 2),((59,36),Freq 2),((59,62),Freq 2),((59,72),Freq 2),((59,78),Freq 2),((59,81),Freq 2),((59,83),Freq 2),((59,85),Freq 2),((59,86),Freq 2),((59,92),Freq 2),((60,2),Freq 2),((60,4),Freq 2),((60,13),Freq 2),((60,18),Freq 2),((60,22),Freq 2),((60,24),Freq 2),((60,32),Freq 2),((60,33),Freq 2),((60,53),Freq 2),((60,63),Freq 2),((60,79),Freq 2),((60,81),Freq 2),((60,82),Freq 2),((60,85),Freq 2),((61,18),Freq 2),((61,24),Freq 2),((61,31),Freq 2),((61,55),Freq 2),((61,62),Freq 2),((61,97),Freq 2),((62,9),Freq 2),((62,17),Freq 2),((62,19),Freq 2),((62,21),Freq 2),((62,23),Freq 2),((62,26),Freq 2),((62,55),Freq 2),((62,64),Freq 2),((62,71),Freq 2),((62,84),Freq 2),((62,94),Freq 2),((63,9),Freq 2),((63,13),Freq 2),((63,51),Freq 2),((63,54),Freq 2),((63,57),Freq 2),((63,64),Freq 2),((63,65),Freq 2),((63,70),Freq 2),((63,77),Freq 2),((63,80),Freq 2),((63,90),Freq 2),((64,8),Freq 2),((64,41),Freq 2),((64,45),Freq 2),((64,49),Freq 2),((64,59),Freq 2),((64,61),Freq 2),((64,98),Freq 2),((65,22),Freq 2),((65,48),Freq 2),((65,56),Freq 2),((65,69),Freq 2),((65,70),Freq 2),((65,87),Freq 2),((66,32),Freq 2),((66,54),Freq 2),((66,79),Freq 2),((66,85),Freq 2),((67,8),Freq 2),((67,9),Freq 2),((67,12),Freq 2),((67,14),Freq 2),((67,43),Freq 2),((67,51),Freq 2),((67,52),Freq 2),((67,58),Freq 2),((67,71),Freq 2),((67,80),Freq 2),((67,96),Freq 2),((68,6),Freq 2),((68,28),Freq 2),((68,51),Freq 2),((68,94),Freq 2),((69,2),Freq 2),((69,12),Freq 2),((69,17),Freq 2),((69,23),Freq 2),((69,41),Freq 2),((69,43),Freq 2),((69,90),Freq 2),((69,93),Freq 2),((70,5),Freq 2),((70,8),Freq 2),((70,20),Freq 2),((70,29),Freq 2),((70,51),Freq 2),((70,52),Freq 2),((70,58),Freq 2),((70,71),Freq 2),((70,94),Freq 2),((71,2),Freq 2),((71,4),Freq 2),((71,9),Freq 2),((71,13),Freq 2),((71,15),Freq 2),((71,32),Freq 2),((71,48),Freq 2),((71,62),Freq 2),((71,68),Freq 2),((71,79),Freq 2),((72,1),Freq 2),((72,6),Freq 2),((72,39),Freq 2),((72,60),Freq 2),((72,64),Freq 2),((72,70),Freq 2),((72,71),Freq 2),((72,87),Freq 2),((73,2),Freq 2),((73,4),Freq 2),((73,46),Freq 2),((73,56),Freq 2),((73,89),Freq 2),((74,9),Freq 2),((74,10),Freq 2),((74,45),Freq 2),((74,46),Freq 2),((74,50),Freq 2),((74,56),Freq 2),((74,57),Freq 2),((74,76),Freq 2),((75,54),Freq 2),((75,68),Freq 2),((75,72),Freq 2),((75,89),Freq 2),((75,99),Freq 2),((76,8),Freq 2),((76,14),Freq 2),((76,52),Freq 2),((76,60),Freq 2),((76,61),Freq 2),((76,74),Freq 2),((76,80),Freq 2),((76,91),Freq 2),((77,27),Freq 2),((77,28),Freq 2),((77,42),Freq 2),((77,59),Freq 2),((77,80),Freq 2),((77,88),Freq 2),((77,90),Freq 2),((78,10),Freq 2),((78,12),Freq 2),((78,33),Freq 2),((78,52),Freq 2),((78,67),Freq 2),((78,68),Freq 2),((78,74),Freq 2),((78,89),Freq 2),((78,95),Freq 2),((78,96),Freq 2),((79,8),Freq 2),((79,10),Freq 2),((79,28),Freq 2),((79,42),Freq 2),((79,51),Freq 2),((79,54),Freq 2),((79,56),Freq 2),((79,60),Freq 2),((79,70),Freq 2),((79,83),Freq 2),((79,96),Freq 2),((80,9),Freq 2),((80,10),Freq 2),((80,27),Freq 2),((80,95),Freq 2),((81,32),Freq 2),((81,33),Freq 2),((81,40),Freq 2),((81,44),Freq 2),((81,56),Freq 2),((81,58),Freq 2),((81,71),Freq 2),((81,92),Freq 2),((82,6),Freq 2),((82,51),Freq 2),((82,86),Freq 2),((83,15),Freq 2),((83,23),Freq 2),((83,25),Freq 2),((83,27),Freq 2),((83,52),Freq 2),((83,61),Freq 2),((83,72),Freq 2),((83,79),Freq 2),((84,12),Freq 2),((84,69),Freq 2),((84,70),Freq 2),((84,76),Freq 2),((84,80),Freq 2),((84,99),Freq 2),((85,9),Freq 2),((85,25),Freq 2),((85,26),Freq 2),((85,49),Freq 2),((85,59),Freq 2),((85,70),Freq 2),((85,87),Freq 2),((85,99),Freq 2),((86,8),Freq 2),((86,20),Freq 2),((86,28),Freq 2),((86,29),Freq 2),((86,43),Freq 2),((86,52),Freq 2),((86,61),Freq 2),((86,62),Freq 2),((86,78),Freq 2),((86,98),Freq 2),((87,43),Freq 2),((87,45),Freq 2),((87,51),Freq 2),((87,58),Freq 2),((88,8),Freq 2),((88,9),Freq 2),((88,12),Freq 2),((88,26),Freq 2),((88,56),Freq 2),((88,60),Freq 2),((88,64),Freq 2),((88,67),Freq 2),((88,81),Freq 2),((88,93),Freq 2),((88,96),Freq 2),((89,8),Freq 2),((89,25),Freq 2),((89,26),Freq 2),((89,28),Freq 2),((89,65),Freq 2),((89,75),Freq 2),((89,84),Freq 2),((89,86),Freq 2),((89,90),Freq 2),((89,91),Freq 2),((90,0),Freq 2),((90,46),Freq 2),((90,88),Freq 2),((90,99),Freq 2),((91,4),Freq 2),((91,5),Freq 2),((91,15),Freq 2),((91,18),Freq 2),((91,70),Freq 2),((91,71),Freq 2),((91,97),Freq 2),((92,21),Freq 2),((92,45),Freq 2),((92,46),Freq 2),((92,74),Freq 2),((92,76),Freq 2),((92,77),Freq 2),((92,87),Freq 2),((93,33),Freq 2),((93,38),Freq 2),((93,58),Freq 2),((93,72),Freq 2),((93,88),Freq 2),((94,4),Freq 2),((94,15),Freq 2),((94,32),Freq 2),((94,44),Freq 2),((94,68),Freq 2),((94,70),Freq 2),((94,79),Freq 2),((94,85),Freq 2),((94,88),Freq 2),((95,1),Freq 2),((95,10),Freq 2),((95,17),Freq 2),((95,27),Freq 2),((95,36),Freq 2),((95,59),Freq 2),((95,84),Freq 2),((95,93),Freq 2),((96,5),Freq 2),((96,10),Freq 2),((96,18),Freq 2),((96,34),Freq 2),((96,38),Freq 2),((96,48),Freq 2),((96,53),Freq 2),((96,71),Freq 2),((96,92),Freq 2),((97,6),Freq 2),((97,19),Freq 2),((97,25),Freq 2),((97,29),Freq 2),((97,36),Freq 2),((97,39),Freq 2),((97,59),Freq 2),((97,60),Freq 2),((97,67),Freq 2),((97,86),Freq 2),((97,96),Freq 2),((98,14),Freq 2),((98,22),Freq 2),((98,32),Freq 2),((98,62),Freq 2),((98,72),Freq 2),((98,85),Freq 2),((98,88),Freq 2),((99,0),Freq 2),((99,12),Freq 2),((99,20),Freq 2),((99,27),Freq 2),((99,40),Freq 2),((99,45),Freq 2),((99,58),Freq 2),((99,61),Freq 2),((99,79),Freq 2),((0,24),Freq 3),((1,61),Freq 3),((2,12),Freq 3),((2,77),Freq 3),((3,96),Freq 3),((4,70),Freq 3),((4,87),Freq 3),((4,94),Freq 3),((4,99),Freq 3),((5,59),Freq 3),((5,94),Freq 3),((6,45),Freq 3),((6,90),Freq 3),((7,24),Freq 3),((7,46),Freq 3),((7,69),Freq 3),((7,72),Freq 3),((8,15),Freq 3),((8,54),Freq 3),((8,56),Freq 3),((8,90),Freq 3),((9,4),Freq 3),((9,14),Freq 3),((9,18),Freq 3),((9,63),Freq 3),((9,79),Freq 3),((9,90),Freq 3),((10,4),Freq 3),((10,18),Freq 3),((10,38),Freq 3),((10,60),Freq 3),((12,35),Freq 3),((12,39),Freq 3),((12,43),Freq 3),((13,19),Freq 3),((13,49),Freq 3),((13,51),Freq 3),((13,67),Freq 3),((14,17),Freq 3),((14,26),Freq 3),((14,83),Freq 3),((14,84),Freq 3),((15,57),Freq 3),((15,64),Freq 3),((15,73),Freq 3),((17,8),Freq 3),((17,14),Freq 3),((17,89),Freq 3),((18,6),Freq 3),((18,8),Freq 3),((18,65),Freq 3),((18,98),Freq 3),((19,18),Freq 3),((19,22),Freq 3),((19,24),Freq 3),((19,88),Freq 3),((20,92),Freq 3),((21,2),Freq 3),((21,10),Freq 3),((21,52),Freq 3),((21,96),Freq 3),((22,9),Freq 3),((22,21),Freq 3),((22,25),Freq 3),((22,51),Freq 3),((22,59),Freq 3),((22,67),Freq 3),((22,75),Freq 3),((23,9),Freq 3),((24,26),Freq 3),((24,29),Freq 3),((24,75),Freq 3),((25,34),Freq 3),((25,49),Freq 3),((25,85),Freq 3),((26,5),Freq 3),((26,54),Freq 3),((26,58),Freq 3),((26,85),Freq 3),((26,91),Freq 3),((27,2),Freq 3),((27,21),Freq 3),((29,35),Freq 3),((29,74),Freq 3),((30,22),Freq 3),((30,38),Freq 3),((30,86),Freq 3),((31,8),Freq 3),((31,25),Freq 3),((32,49),Freq 3),((32,91),Freq 3),((33,9),Freq 3),((33,69),Freq 3),((34,17),Freq 3),((34,19),Freq 3),((35,4),Freq 3),((36,49),Freq 3),((36,58),Freq 3),((36,61),Freq 3),((36,88),Freq 3),((36,96),Freq 3),((37,28),Freq 3),((37,67),Freq 3),((37,81),Freq 3),((38,10),Freq 3),((38,25),Freq 3),((38,59),Freq 3),((38,67),Freq 3),((38,86),Freq 3),((38,96),Freq 3),((38,98),Freq 3),((39,4),Freq 3),((39,13),Freq 3),((40,69),Freq 3),((40,83),Freq 3),((41,63),Freq 3),((43,4),Freq 3),((43,24),Freq 3),((43,60),Freq 3),((44,84),Freq 3),((45,54),Freq 3),((45,71),Freq 3),((46,12),Freq 3),((46,30),Freq 3),((46,55),Freq 3),((46,60),Freq 3),((46,98),Freq 3),((48,7),Freq 3),((49,32),Freq 3),((49,34),Freq 3),((49,37),Freq 3),((49,56),Freq 3),((51,13),Freq 3),((51,14),Freq 3),((51,34),Freq 3),((51,54),Freq 3),((51,63),Freq 3),((51,70),Freq 3),((52,22),Freq 3),((52,55),Freq 3),((52,60),Freq 3),((52,62),Freq 3),((52,89),Freq 3),((54,10),Freq 3),((54,60),Freq 3),((54,71),Freq 3),((54,80),Freq 3),((54,86),Freq 3),((55,72),Freq 3),((55,79),Freq 3),((56,10),Freq 3),((56,17),Freq 3),((56,25),Freq 3),((57,1),Freq 3),((57,97),Freq 3),((57,99),Freq 3),((58,15),Freq 3),((58,18),Freq 3),((58,22),Freq 3),((59,32),Freq 3),((59,38),Freq 3),((59,40),Freq 3),((59,54),Freq 3),((59,76),Freq 3),((59,91),Freq 3),((60,31),Freq 3),((60,40),Freq 3),((61,60),Freq 3),((61,68),Freq 3),((61,79),Freq 3),((61,88),Freq 3),((62,35),Freq 3),((62,69),Freq 3),((63,67),Freq 3),((63,83),Freq 3),((64,90),Freq 3),((64,93),Freq 3),((65,33),Freq 3),((65,46),Freq 3),((66,68),Freq 3),((66,89),Freq 3),((68,59),Freq 3),((68,75),Freq 3),((69,52),Freq 3),((69,81),Freq 3),((70,42),Freq 3),((70,59),Freq 3),((70,72),Freq 3),((71,5),Freq 3),((71,18),Freq 3),((71,37),Freq 3),((71,63),Freq 3),((71,72),Freq 3),((71,74),Freq 3),((71,88),Freq 3),((72,8),Freq 3),((72,28),Freq 3),((72,42),Freq 3),((72,94),Freq 3),((72,99),Freq 3),((73,13),Freq 3),((73,72),Freq 3),((73,90),Freq 3),((74,6),Freq 3),((74,21),Freq 3),((74,69),Freq 3),((74,70),Freq 3),((74,77),Freq 3),((74,83),Freq 3),((75,37),Freq 3),((77,51),Freq 3),((79,7),Freq 3),((79,84),Freq 3),((79,98),Freq 3),((80,90),Freq 3),((82,28),Freq 3),((82,45),Freq 3),((83,20),Freq 3),((83,29),Freq 3),((84,6),Freq 3),((84,29),Freq 3),((84,86),Freq 3),((84,87),Freq 3),((85,29),Freq 3),((86,12),Freq 3),((86,41),Freq 3),((86,71),Freq 3),((86,81),Freq 3),((87,8),Freq 3),((87,59),Freq 3),((87,71),Freq 3),((88,28),Freq 3),((88,84),Freq 3),((88,98),Freq 3),((89,17),Freq 3),((89,96),Freq 3),((90,62),Freq 3),((91,24),Freq 3),((92,35),Freq 3),((94,2),Freq 3),((94,5),Freq 3),((94,13),Freq 3),((94,22),Freq 3),((94,24),Freq 3),((94,31),Freq 3),((95,21),Freq 3),((96,32),Freq 3),((96,33),Freq 3),((96,73),Freq 3),((97,55),Freq 3),((97,66),Freq 3),((98,13),Freq 3),((98,34),Freq 3),((99,43),Freq 3),((99,71),Freq 3),((99,98),Freq 3),((0,86),Freq 4),((1,49),Freq 4),((4,84),Freq 4),((6,2),Freq 4),((8,38),Freq 4),((9,55),Freq 4),((10,68),Freq 4),((10,88),Freq 4),((14,35),Freq 4),((15,9),Freq 4),((15,28),Freq 4),((15,35),Freq 4),((15,42),Freq 4),((17,4),Freq 4),((18,45),Freq 4),((22,17),Freq 4),((22,39),Freq 4),((22,98),Freq 4),((24,94),Freq 4),((27,5),Freq 4),((28,38),Freq 4),((28,89),Freq 4),((29,9),Freq 4),((31,45),Freq 4),((32,59),Freq 4),((33,57),Freq 4),((35,60),Freq 4),((36,51),Freq 4),((37,21),Freq 4),((37,36),Freq 4),((37,70),Freq 4),((37,76),Freq 4),((37,99),Freq 4),((40,3),Freq 4),((40,17),Freq 4),((40,86),Freq 4),((42,10),Freq 4),((42,74),Freq 4),((43,66),Freq 4),((43,72),Freq 4),((45,68),Freq 4),((46,71),Freq 4),((48,8),Freq 4),((49,15),Freq 4),((50,82),Freq 4),((52,66),Freq 4),((56,32),Freq 4),((58,63),Freq 4),((59,64),Freq 4),((60,14),Freq 4),((61,19),Freq 4),((61,44),Freq 4),((62,59),Freq 4),((62,77),Freq 4),((62,98),Freq 4),((63,7),Freq 4),((63,21),Freq 4),((63,69),Freq 4),((64,96),Freq 4),((65,37),Freq 4),((66,18),Freq 4),((66,44),Freq 4),((66,63),Freq 4),((70,27),Freq 4),((70,61),Freq 4),((71,22),Freq 4),((73,62),Freq 4),((74,86),Freq 4),((77,45),Freq 4),((79,17),Freq 4),((80,35),Freq 4),((80,73),Freq 4),((81,38),Freq 4),((81,54),Freq 4),((83,81),Freq 4),((83,96),Freq 4),((84,77),Freq 4),((85,45),Freq 4),((86,45),Freq 4),((87,52),Freq 4),((88,41),Freq 4),((88,86),Freq 4),((90,22),Freq 4),((90,48),Freq 4),((92,7),Freq 4),((92,65),Freq 4),((94,7),Freq 4),((94,34),Freq 4),((94,40),Freq 4),((96,13),Freq 4),((96,22),Freq 4),((96,54),Freq 4),((96,95),Freq 4),((97,17),Freq 4),((97,51),Freq 4),((99,59),Freq 4),((2,3),Freq 5),((2,69),Freq 5),((8,85),Freq 5),((8,97),Freq 5),((9,96),Freq 5),((12,9),Freq 5),((14,98),Freq 5),((15,45),Freq 5),((17,62),Freq 5),((23,35),Freq 5),((24,17),Freq 5),((24,59),Freq 5),((27,15),Freq 5),((28,88),Freq 5),((29,37),Freq 5),((35,88),Freq 5),((37,77),Freq 5),((37,86),Freq 5),((37,87),Freq 5),((38,51),Freq 5),((40,12),Freq 5),((43,62),Freq 5),((45,22),Freq 5),((48,26),Freq 5),((49,31),Freq 5),((51,2),Freq 5),((59,68),Freq 5),((60,88),Freq 5),((63,75),Freq 5),((68,57),Freq 5),((69,29),Freq 5),((69,59),Freq 5),((69,61),Freq 5),((71,38),Freq 5),((77,61),Freq 5),((81,63),Freq 5),((83,43),Freq 5),((83,49),Freq 5),((83,98),Freq 5),((88,91),Freq 5),((91,38),Freq 5),((96,9),Freq 5),((96,15),Freq 5),((98,2),Freq 5),((12,10),Freq 6),((17,79),Freq 6),((29,10),Freq 6),((37,64),Freq 6),((37,69),Freq 6),((40,96),Freq 6),((42,35),Freq 6),((42,73),Freq 6),((63,84),Freq 6),((63,99),Freq 6),((68,45),Freq 6),((74,99),Freq 6),((84,83),Freq 6),((91,63),Freq 6),((96,97),Freq 6),((98,40),Freq 6),((2,96),Freq 7),((17,94),Freq 7),((45,15),Freq 8),((96,35),Freq 8),((37,83),Freq 9),((39,37),Freq 9),((45,38),Freq 9),((59,63),Freq 9),((73,37),Freq 10),((9,37),Freq 11),((35,37),Freq 13),((10,37),Freq 15),((98,63),Freq 18)]"
-}

{-
30000 0.1223275859827659

Table {getTable = fromList [(0,Just '\1043'),(1,Just '\1096'),(2,Just '\1045'),(3,Just '\1086'),(4,Just '\1067'),(5,Just '\1066'),(6,Just '\1094'),(7,Just '\1087'),(8,Just '\1087'),(9,Just '\1096'),(10,Just '\1074'),(12,Just '\1080'),(13,Just ' '),(14,Just '\1050'),(15,Just '\1042'),(16,Just '\1085'),(17,Just '\1073'),(18,Just '\1069'),(19,Just '\1082'),(20,Just '\1079'),(21,Just '\1103'),(22,Just '\1041'),(23,Just '\1089'),(24,Just '\1068'),(25,Just '\1089'),(26,Just '\1099'),(27,Just '\1093'),(28,Just '\1075'),(29,Just '\1097'),(30,Just '\1088'),(31,Just '\1053'),(32,Just '\1060'),(33,Just '\1058'),(34,Just '\1065'),(35,Just '\1074'),(36,Just '\1057'),(37,Just '\1101'),(38,Just '\1059'),(39,Just '\1102'),(40,Just '\1047'),(41,Just '\1076'),(42,Just '\1083'),(43,Just '\1083'),(44,Just '\1040'),(45,Just '\1100'),(46,Just '\1098'),(47,Just '\1077'),(48,Just '\1081'),(49,Just '\1073'),(50,Just '\1072'),(51,Just '\1091'),(52,Just '\1090'),(53,Just '\1081'),(54,Just '\1055'),(55,Just '\1085'),(56,Just '\1063'),(57,Just '\1072'),(58,Just '\1084'),(59,Just '\1091'),(60,Just '\1078'),(61,Just '\1090'),(62,Just '\1048'),(63,Just '\1044'),(64,Just '\1094'),(65,Just '\1095'),(66,Just '\1076'),(67,Just '\1097'),(68,Just '\1062'),(69,Just '\1046'),(70,Just '\1049'),(71,Just '\1082'),(72,Just '\1071'),(73,Just '\1075'),(74,Just '\1102'),(75,Just '\1078'),(76,Just '\1080'),(77,Just '\1103'),(78,Just ' '),(79,Just '\1070'),(80,Just '\1093'),(81,Just '\1099'),(82,Just '\1101'),(83,Just '\1051'),(84,Just '\1077'),(85,Just '\1056'),(86,Just '\1054'),(87,Just '\1098'),(88,Just '\1064'),(89,Just '\1052'),(90,Just '\1100'),(91,Just '\1084'),(92,Just '\1092'),(93,Just '\1095'),(94,Just '\1086'),(95,Just '\1092'),(96,Just '\1079'),(97,Just '\1040'),(98,Just '\1088'),(99,Just '\1061')]}

 П  Э  Ч  А  Р  Й  Ю  В  А  Н  Х  Л  Д  Ф  Ч  Ю     Э  Р  С  У  А  К  Ю  Ц  Р  Й  И  Р  З 
 И     В  Д  Ы  Е  И  Ш  Д  Е  Й  Н  Э  Р  И  З  П  Б  Ы  Я  У  П  Л  Г  Ц  Ц  Е  Ж  У  Ф 
 К  Ю  Ч  Ц  Д  Ш  Г  И  У  Б  Э  В  Ф  Ъ  Л  Ю  Щ  Р  Ы  Ю  Э  Р  З  О  У  Ч  Д  Я  Т  Н 
 Й  Щ  П  В  А  Й  О  Ы  У  Щ  Н  Ф  Ч  И  П  Ц  Ж        Ш  И  Я  Б  Щ  К  М  Ъ  М  Щ  И 
 У  К  Б  Я  В  Н  Г  И  Т  Ж  К  И  В  Ы  Ъ  М  Л     В  В     Ь  Т  Ш  Э  Я  Х  Ь  Ь  О 
 И  Ж  У  Р  К  В  З  Ъ  В  Ю  Ъ  Ф  У  З  Г  Ъ  М  Б  П  Ч  Я  Ц  Р  Д  Ж  Я  Ц  Ы  Р  Щ 
 Х  Л  Г  А     З  Ф  В  Э  Й  Х  Ь  Г  Ю  Ь  Э     Ъ  И  Ъ     Ч  М  А  С  Б  Й  У  Ъ  У 
 Ф  Д  М  У  Ц  С  В  Ы  Ц  Р  Д  Ю  Ы  И  Р  Д     Ж  Л  Ж  Я  Л  Г  Я  Ъ  П  Х  Ю  Ф  Ъ 
 Р  Ч  Д  Я  У  Д  Х  Ь  Щ  В  У  Ю  Й  Щ  Э  Я  Б  О  Р  Б  Ч  Й  Р  Е  П  Э  Л  Т  Ш  Р 
 Ю  Й  Ы  Р  Ъ  Р  Ж  Н  Ъ  Д     Ф  Ю  Ф  Ь  Ц  Н  Ф  Й  Ъ  М  А  Ъ  Ч  Ж  С     В  М  Е 
 Ъ  Б  Ч  Ю  З  С  Ф  С  У  Я  Р  Т  Ю  Э  М  Д  Ф  Ъ  Г        Ю     Е  К  М  Д  Я  А  А 
 З  Ъ  Ч  Т  Х  Ю  З  Г  Ф  Б  Э  Л  Л  Ж  Ю  П  Ш  Й  Т  И  С  Ш  Ю  У  Е  Ъ  Ы  У  У  Л 
 Л  Д     Ф  С  Ч  Ш  Щ  Э  О  З  Л  В  Н  М  Ц  Д  Ч  Ч  Т  Ь  У  В  Э  Я  Ь  Д  И  Ю  Г 
 Г  З  О  Ц  Ь  Ы  Щ  С  Б  О  Ч  О  Я  В  К  В  А  М  К  З  Ш  С  Г  И  Я  Ь  У  Ы  Ю  А 
 Ш  Ц  Д  Р  Щ  Э  Я  И  Ю  Г  П  Г  П  Е  О  А  Ф  Е  Ъ  О     Б  Э  Ж  Т  Ш  И  Ю  Р  С 
 Б  Н  С  Щ  Б  М  Ж  Т  Н  Т  Р  Д  Ш  Ч  Ъ  Ю  О  Ь  Б  Г  А  Я  Х  Ъ  З  Г  А  З  Ф  Н 
 З  Ц  Е  Ш  М  В  И  Л  Р  Д  Ч  Ж  Х  У  Б  Э  Ч  Х  Д  Ш  Ъ  Ы  Г  Щ  К  Б  Щ  Ш  Х  Г 
 Ш  М  Ч  Ф  З  К  А  Я  Ы  А  С  Л  И  Ф  Б  Н  У  Г  Э  Ъ  К  Ш  Ц  Л  Ы  З  В  З  О  И 
 Х  Б  З  В  В  Э  С  У  Г  Ь  Ы  М  Я  Ш  Л  Н  Л  У  Ъ  К  В  Ь  Ж  Р  С  Ъ  А  Ш  У  Ы 
 Ч  П  Я  Т  Ь  Ж  Э  Ь  Ш  Д  В  В  Э  Л  Ю  Я  С  В  Ф  У  Х  Г  Я  Г  М  С  Р  И  У  Ш 
 Ч  Я  Х  Ь  М  П  С  Б  Ъ  Ч  Е  Ж  У  Т  Ж  Я  Ы  Ь  Я  Е  И  Ш  У  Ы  А  Ч  Т  А  Щ  Щ 
 В     Д  Ю  Ч  Б  Ф  Р  Ъ  Ж  Б  У  Х  Ъ  Ъ  Ъ  О  Ж     Ы  Ъ  У  И  Ж  Б  Р  К  В  Д  Щ 
 З  Т  Д  М  З     Г  У  И  И  М  Й  Л  К  У  Г  Ь  Е  Я  Ы  Р  Х  З  И  В  К  Ы  Й  А  Х 
 Р  Г  Щ  Х  Т  К  А  Д  К  П  М  Ч  Е  О  Х  Л  А  Г  Э  Я  Р  Ы  Ш  К  Ж  Н  Ю  Ж  П  Ь 
 Ч  В  Э  Х  Ъ  Ж  У     Ф  О  Ю     К  М  К  Л  К  И  Ж  Д  Б  Р  И  Ш  Б  Ю  Б  Я  В  Ц 
 Р  О     З  В  Ю  Й  Ю  Ц  Ь  Л  М  Ч  К     Я  У  К  Б  Я  Ъ     И  Л  И  Ю  Э  Ь  Я  З 
 В  Г  Э  Х  П  Ч  Ю  Ш  Д  Э  Ь  К  Д  П  Ш  Ь  В  П     Э  З  Ж  Щ  Д  Ь  Г  Е  И  Л  Ч 
 Н  М  С  Ф  Г  К  Ц  Л  Х  Ж  П  О  З  О  А  Т  Е  Ъ  П  П  К  Э  Й  Т  Ц  Т  В  Й     Л 
 Х  В  Ь  В  А  Ш  З  В  Ы  Щ  М  Э  У     Ш  К  У  П  И  Я  П  А  Э  Л  Б  Щ  У  М  М  Б 
 Ю  С  Т  А     К  Э  Г  Р  А  Ш  У  В  Э  Ц  Б  Н  П  Я  Ю  Р  З  И  Ш  Д  А  Е  Щ  Г  Ь 
 И  У  Щ  К  У  С  Ф  Б  Ю     Щ  С  В  Э  Р  Б  С  Т  Ь  Л  Р  Б  Б  Б  Ы  М  Ъ  Г  Б  Щ 
 К  М  Л  О  Ю  Л  В  Ц  А  Ц  Р  Ш  Ю  Э  С  Е  О  Ж     Р  Ю  Ж  Ш  Р  Г  З  Ж  Ч  Ц  Р 
 Ю  Г  И  Ж  Ы  Е  Л  Я  Г     Ц  Ь  В  Ж  Я  Х  Э  Х  У  М     М  Ч  Б  К  Я  У  Э  А  А 
 Б  Э  Г  Т  П  Й  З  Й  П  А  Ы  Г  Э  У  А  Ж  Й  Ф  А  Э  О  Г  Н  Х  Й  П  Г  Г  Я  Й 
 Д  Н  Ь  К  Я  К  Р  Д  Ц  Ь  Ы  Д  П  Ц  Е  Х  Ш  Н  Ю  Е  И  Г  Д  Я  Ф  Я  Х  Е  З  Э 
 Ц     Т  Н  Ь  З  Я  О  Ы  Д  Л  З  Щ  Э  Ж  С  Г  К  Ц  Ю  М  Ш  Д  Д  А  К  Ы  Ъ  Ч  Г 
 О  Ы  У  Й  Т  А  Ч  Й  Ъ  Л  Л  Ж  К  И  Т  А  Ъ  У  Ч  Ф  М  Ю  Л  Ж  В  Ь  Ж  М  Ы  Й 
 Г  Э  Ж  Д  Й  П  Р     Ь  Р  У  У  Р  Ж  Д  Х  И  Ш  О  Щ  Г  Ч  Э  К  Я  С  О  Б  Ш  З 
 Ф  Р  Й  Ц  П  Ь  Й  Ы  Щ  К  Ъ     Ю  Ц  Ь  Ш  П  Ч  Ф  Ж  Э  Ы  А  Й  Ю  Э  Ь  А  Н    
 М  Д  П  Ъ  З  Р  Э  Ь  Р  Ъ  О  З  Ж  Г  Ж  Ф  В  Э  О  Ц  Х  Е  З  Х  Е  Я  В  Л  З  Х 
 Э  Я  Ш  И  В  Ц  Ч  Э  К  Ю  П  Ц  Д  Г  С  З     Ш  Ь  Э  К  Ш  З  О  Ъ  И  У  У  Х  Я 
 Ы  Я  М  Э  З  Б  Ю  Щ  Г  К  Б  Х  Т  З  Ш  Д  Ь  Б  Г  Э  Щ  Т  Ж  Ш  Ч  Ш  П  Э  Ю  Л 
 Ы  Ф  Щ  У  Ь  Б  Щ  Т  Ь  Б  Ю  Ч  В  Э  Щ  З  Ш  Я  Ц  М  М  Х  Ь  Э  П  З  У  Ц  Ж  Х 
 К  И  Ц  Л  Л  Д  Э  Ч  С  Ф  Ч  Э  В  Э  Ъ  Т  Ш  Г  Б  С  К  Ф  Ж  Л  Г  У  Ч  Ъ  Ж  А 
 С  Й  Ч  Ъ  Т  А  Л  У  И  В  Д  Р  В  Э  Я  Ф  Ц  Т  Ь  Г  З  З  Ъ  В  Э  Щ  Ю  Я  Р  М 
 Б  П  У  Я  Л  О  Щ  Ш  Ж  Ш  Й  К  Ш  Г  Ч  Р  Я  Е  С  З  Х  В  Ж  Ш  Е  Х  В  З  О  Р 
 Д  Ж  М  Щ  Ь  В  Г  Ш  Ы  З  П  М  Ш  Щ  Л  В  Т  Н  Л  Ч  Ь  Ю  Ч  Ж  Ч  Й  И  В     С 
 Р  Е  У  Б  Б  Т  Ж  Ф  Ъ  Р  Д  Х  В  К  Ы  Й  Б  В  Г  Х  Ъ  О  Ц  А  Ъ  Л  Ю  Б  К  Х 
 К  У  С  Р  Б  Ш  Ш  Е  З  В  Л  В  Р  У  Ж  Ь  Ч  М  Г  Н  З  Б  Ш  П  Н  З  Й  И  М  М 
 Щ  Ь  Б  К  Н  Ы  Ф  З  Т  А     И  Ю  Х  В  Ь  Ю  Э  Г  К  Я  Ь  Н  П  К  Й  У  Л  Ю  Б 
 И  Д  А  О  Ш  У  О  Ъ  Я  З  Ч  Й  Н  Ф  Е  Л  В  Я  Л  Н  Ч  О  О  Й  С  Л  Щ  К  П  П 
 Ы  Й  Е  Ц  И  Б  И  Н  Ю  П  О  К  Ъ  Л  Ь  Ь  Б  В  Э  Л  Б  А  С  Щ  В  У  Ч  Ч  Ь  У 
 В  Ф  У  Г  Ш  Т  Э  Т  Б  Я  Л  Х  В  Ь  Ц  А  Щ  З  Ш  К  Ц  У  Д  Х  Г  Р  З  П  П  У 
    Щ  П  Щ  А  Ц  В  Э  Ц  Х  Ъ  И  В     У  Е  Х  Р     Ь  Ь  Т  В  Л  Б  Я  Л  Г  Г  Й 
 Я  Б  Л  Ы  У  З  Ы  Э  У  Й  Л  Ц  К  Р  Е  Щ  В  К  А  Е  Щ  В  З  Э  Я  Т  А  Н  Ъ  У 
 Р  В  Д  А  Ч  И  Е  Л  М  П  Д  Ц  Б  Ч  М  Ш  Е  О  И  Ъ  Х  В  Р  У  Д  Ц  А  М  Ж  Т 
 Ж  Т  П  З  Ф  Ы  Е  А  Ц  Г  Я  Х  У  Ы     Г  Й  Щ  П  Р  У  З  О  К  Э  С  А  Б  И  Я 
 Е  З  У  Л  Г  Х  Н  Л  Т  Ш  Л  Ж  Ч  Ф  П  Ч  Ь  Ы  Ф  Ж  Х  Т  Ы  П  З  У  У  В  Э  Ж 
 Ы  Э  П  Щ  Ш  А  У  Ж  Б  О  Ф  Ъ  Н  В  Щ     Л  К  Ж  К  Ы  Ъ  Ш  Э  Ъ  Ь  Й  П     К 
 Х  У  У  П  К  Д  Й  Ъ  Я  П  У  Ш  З  Т  Г  Ф  Ш  О  З  Й  Ы  Ф  Б  Ф  Ю  Х  У  Х  Б  П 
 А  Ж  Ч  З  И  В  Ж  К  Ф  В  Э  Й  Х  И  К  Ш  К  Ь  Ю  Э  О  Т  Р  Д  Ф  Х     З  Щ  Ы 
 Ф  Ю  Ъ  Ж  Е  Р  Ф  Ы  Б  Ш  Т  К  Ф  Ъ  Т  Ю  У  К  А  Т  С  Ч     Ц  Ь  П  Н  К  Ч  Ю 
 О  К  Ф  Х  Ь  Б  П  Н  Ц  О  У  Ы  Р  Ш  К  А  О  Ь  Ц  О  Е  У  Д  З  В  А  Щ  Ю  Й  Я 
 Л  Г  В  Х  Ф  П  Г  О  Д  Ь  Т  К  Ь  Щ  Ц  П  Л  У  И  Т  Ю  Ч  В  К  У  Е  Й  Щ  Т  Д 
 Ь  Л  Ф  Ч  Й  Ь  Б  З  П  У  И  М  У  У  О  Л  Т  Л  В  А  У  У  Щ  Р  Р  Ь  И  Т  Б  Щ 
 Б  Ч  Ш  Л  В  Ъ  Ж  Ы  З  О  Ь  К  Б     С  Э  Й  У  П  Х  Г  Н  Ц  Ь  П  А  Щ  Э  О  П 
 Ч  Й  Х  Ф  А  Щ  У  Б  Ж  Ы  С  Ж  Т  Ъ  З  Г  И  Х  У  Я  Ы     В  Л  Щ  Ж  З  З     Т 
 Г  Э  Х  Ю  Ж  Т  Н  Ъ  О  Э  Г  Ъ  Р  В  Ш  М  Ъ  Р  Е  О  Э  Л  Ы  П  Ь  Ь  Б  Ы  К  Д 
 Е  Й  М  Э  П  К  Я  З  Л  А  Б  О  Ц  Ц  Р  Д  Г  Э  Щ  Б  Ы  Г  В  Ш  Д  Д  У  Й  Г  Г 
 О  Б  Ф  К  Ы  Ф  П  Г  В  Ы  М  Ш  Х  Ш  Ю  У  Ш  Б  Ь  Ж  У  Д  Х  Ы  П  О  К  Ф  Ч  Я 
 У  К  Ч  Ы  Ф  У  Я  Л  Я  Л  В  Я  Ю  Б  М  Ю  Л  П  Р  Ы  З  Ж  Й  Ш  Э  Ж  Л  И  С  В 
 Ь  У  З  Ц  Т  К  Ш  П  М  Ж  Ы  Ф  У  Щ     Ю  Х  К  У  Ц  Ш  Г     П  Б  Т  З  Ф  И  Г 
 Ф     И  Х  К  Ф  П  Х  Ч  В  Ч  У  О  Ю  П  Ж  Э  М  Д  Ю  Я  Т  Ы  О  З  У  У  А  Х  З 
 В  Ш  О  Ф  К  Ю  Б  О  Н  Щ  Е  С  Ш  В  Н  Э  Я  Е  Ы  Х  Ю  З  А  Ы  Э  Л  Щ  Й  Ч  Э 
 Ы  Ч  К  И  Ь  Г  Ж  О  З  Н  Р  Ш  Л  Ю  И  Т  Н  К  Э  К  К  Е  Ю  С  М  А  Я  Т  Ш  В 
 Ь  У  Ш  К     Р  С  Т  Ж  Ц  З  Г  Ы  Щ  Ч  З  З  Ч  С  Р  Л  Р  Д  Ш  Э  Л  Б  Н  Б  Ь 
 В  Ш  Д  Ф  Ж  Й  Ж  Ь  В  Р  Н  Ж  Т  К  Б  Д  С  У  Д  Ж  В  Е  В  Я  Ч  О  Д  Ш  Э  У 
 Й  К  Ъ  Р  Г  М  О  Д  Г  Х  М  Р  К     Е  Б  П  Ш  Б  В  Х  С  Б  Ф  Ц  Ь  М  У  Ф  В 
 О  О     Е  Щ  Ш  Ь  Й  В  А  Ю  Э  М  Г  Ь  Й  У  У  Ж  И  Я  Ч  Ф  Ъ  Ч  З  Ш  Ы  Ю  Ц 
 Б  У  З  В  Ч  Ъ  Н  Н  Ь  У  Д  Г  С  Ю  Ю  Ш  Э  Ж  К  В  О  Й  Л  Я  И  В  Э  Ь  Ч  Р 
 Б  К  Ы  Е  О  Ь  У  П  В  З     Е  Л  Х  В  Г  Я  Ю  Й  Т  И  К  Ю  А  Ш  Ч  Ъ  О  Щ  М 
 Г  У  У  П  Г  Д  Д  Х  М  Ф  И  Р  П  Я  О  Ъ  У  Д  И  П  Ж  Ю  Х  Ь     Н  Л  Ф  Х  Ф 
 С  К  Р  Л  В  Э  М  Ы  З  Г  Ъ  Я  В  Ц  У  Я  Б  О  Р  Ъ  У  Д  Ж  Щ  Н  У  Ф  Ъ  К    
 Й  У  Ь  Ы  П  У  Ц  К  Р  П  Х  Г  Т  Щ  З  Ф  В  Э  И  Т  М  Х  Г  М  Ю  М  К  М  Б  Ю 
 Л  М  У  Р  Ч  О  П  Ф  М  Т  Ю  Х  М  У  Д  А  Ж  Ы  И     Й  Я  О  К  Е  Я  Г  У  У    
 Ж  З  Щ  К  Б  О  А  Ж  Т  И  З  А  Щ  Й  Г  Ч  Э  Х  Ч  К  Г  А  Ю  Я  Ъ  У  Н  С     Т 
 Ы  Д  Я  З  В  И  Р  Д  Й  А  П  Н  В  Б  Ю  В  Э  Ц  Р  Э  О  З  Ш  Ь  Н  Е  О  Я  Т  Ю 
 Д  А  Х  Й  У  С  Ш  Г  М  Ж  Ч  И  Ц  Ь  П  Я  П  В  Л  З  Й  О  Ь  У  Ц  З  В  Ш  Р  Д 
 Ц  З  Ф  О  П  В  Э  И  Х  В  Ы  Х  Т  К  О  П  Ь  О  Ъ  В  Ъ  Х  Й  К  Щ  У  Е  Ж  В  Ж 
 П  В  Э  О  Г  Ж  Ю  И  Д  П  С  Й  А  Ъ  К  Б  И  Л  Я  Ю  Ы  Ы  Й  В  Э  Ш  Д  Х  Л  Н 
 Ю  П  Ы  П  П  О  Г  Я  К  Б  С  Р  У  З  Я  С  В  Ч  В  Р  З  Л  В  Я  З  Р  Ш  З  Э  Н 
 Г  Е  О  Ц  Ь  Х  Р     Ж  Э  Ь  Г  У  П  Т  У  Т  Б  Щ  Х  Х  Н  Ь  Н  А  С  З  В  Ш  Я 
 Ь  Ф  Г  О  Ф  И  Р  Б  У  Щ  Д  Д  Ж  З  Я  Ы  П  Б  И  Ж  Щ  В  Э  К  Д     Х  И  Т  Ф 
 Л  Э  Ы  К  Д  А  Ц  Щ  Щ  Ч  Р  Ц  Г  Ю  Б  Ю  В  Ш  О  Ч  П  Ж     Ч  М  А  Б  Ю  И  Ь 
 Э  Ь  И  О  Н  Ъ  К  В  С  Х  К  В  А  Ж  К  У  С  Ш  М  Ъ  Х  Ь  Ч  Б  О  В  В  Ы  Й  О 
 Щ  Ю  Э  Ж  С  Л  Ъ  Щ  В     У  Е  Л  Р  Ф  Ь  Я  Л  Н  Щ  М  Ы  Л  В  Ф  Л  Б  И  Ъ  Д 
 Л  Ь  Ч  М  А  Ъ  Н  Н  Р  М  З  Й  А  Ц  М  Я  Х  Р  О  Ы  Т  У  Ъ  М  Е  Ы  М  Я  Ц  Л 
 Ь  Щ  Ш  В  Ь  Г  Т  Г  Э  Я  П  В  Ш  З  Ф  Х  Ф  И  О  Ь  Й  Ы     Б  Т  Ю  Л  З  Г  Ы 
 Р  Д  Х  З  Г  Ю  Ч  О  З  В  Ш  Э  Л  Ь  У  Т  Й  Щ  Э  И  Д  Ф  П  Р  М  Х  Л  Ы  Ъ  Л 
 Ф  Б  М  Н  Ч  Е  Ж  Щ  Щ  С  В  Й  П  Щ  О  Б  В  З  Т  Х  Ы  Ч  Х  С  Ш  Ы  Й  Г  Т  М 
 Ь  Ч  Я  Г  А  Ф  В  Ж  Х  С  Г  Ш  М  Ь  У  Т  Ш  Э  Я  У  М  Ь  Б  К  Л  С  А  А  Г  В 
 К  Ш  С  Ы  П  Ж  Б  Ф  Ф  Я  М  К  Ж  Щ  Ц  У  Ф  И  П  Б  В  З  Й  Г  Й  Л  Ы  Ц  О  Ь 
 Ъ  Ж  Н  Г  Л  Л  Ы  А  Б  Б  Н  В  Э  И  М  Б  Х  Т  Ь  О  П  Я  П  Р  Х  Х  Я  Ь  А  М 
 Щ  Я  Ы  Г  П  Ь  Б  Ш  Д  Ч  Ц  А  У  К  Е  Я  Ч  Й  П  Р  Ю  Х  Э  У  Ф  Д  Ю  Е  Я  З 
 Ш  К  Р  Д  Я  Ш  Ч  Б  В  И  Е  И  Л     Ю  В  Ц  Ч  Х  Ю  О  З  Ф  В  Э  Л  Ю  Й  Х  Я 
 В  А  У  З  Ъ  Ч  Д  А  У  Ф  Б  Ф  Я  Г  Ю  Б  О  Б  Я  Т  М  Г     Б  Н  Й  Б  Ю  Э  Ы 
 Д  Е  Ъ  Б  Н  К  Е  С  Ф  В  Щ  Ю  Э  Л  Т  А  Г  О  Ш  Е  О     В  Й  Х  Я  С  В  Ы  Е 
 Я  Ь  Ш  М  Р  Ь  Б  В  Ф  У  И  Ю  Ж  И  Ш  Э  Ч  Ъ  О  Е  У  В  Г  Э  С  М  Б  О  Й  Ч 
 Б  Ч  Ь  П  К  М  Д  У  Т  Л  А  Е  З  Б  У  Й  С  Ы  К  Й  Ъ  П  Я  Ч  Ы  М  Д  Ж  Т  Ч 
 Н  Я  Д  П  Н  Ю  Ш  Э  Г  О  Т  Д  Ш  Ч  С  Э  А  Л  Ж  Й  Ш  Г  В  Я  М  Й  Р  Е  П  Э 
 Ц  З  А  Н  Р  Ь  Ъ  Ч  Т  Ы  Т  О  Я  Ъ  Ц  А  И  Ю  Ф  Й  Х  Ш  Ю  В  М  Ъ  Л  Ч  А  Д 
 П  И  Э  Г  Ъ  Ж  Ц  Ь  П  Ж  Ы  Д  Е  Л  Ю  Е  З  И  Г  И  Я     Т  Ж  Ы  О  Н  Ч  В  В 
 Э  О  В  Ж  Р  Ч  Ш  Ы  Ы  Д  Е  Щ  И  Ъ  Б  К  Ц  Ж  О  Ш  А  Щ  Р  Щ  Г  Э  Я  Ф  П  Ъ 
 М  У     О  Ы  Х  Х  В  У  З  Ф  Ш  Э  С  М  У  Ц  П  Ю  Й  Ы  М  Д  П  С  Б  Д  Э  Ы  Й 
 Н  Б  П  Ъ  Б  У  С  Щ  Б  Ю  Ъ  З  З  Э  Х  В  Г  У  И  Ю  Х  М  Э  К  Т  И  К  Ч  Б  Ж 
 Л  З  А  В  Д  Ж  Ф  Я  Ю  Т  М  Й  Ф  С  Т  Ю  Л  П  Д  Э  Г  Ц  Т  Ж  Ш  П  Ь  О  Э  Ц 
 Х  Ъ  К  Б  С     У  Е  Л  С  Ш  Б  Р  З  Ж  У  А  Ы  О  Е  Ж  О  Ь  У  Ц  У  Т  Ж  Ь  О 
 Ю  Ж  Э  Ц  Ь  В  П  Ы  Ф  Б  А  И  К  Б  И  Н  Я  П  У  Щ  Л  Щ  М  Р  Щ  Й  П  Й  Л  Ш 
 Я  Ж  Г  Ь  М  Ц  Й  Ц  Ь  Ъ  Р  Щ  Ж  Э  Ь  Р  Э  П  Ъ  Л  И  Л  Щ  К  Э  Г  О  И  Б  А 
 Н     М  П  У  Л  Я  И  Х  Ь  Ш  Ф  У  Л  З  Ф  К  В  Щ  Ы  Ю  У  Ц  Ю  Ъ  Т  М  О  А  С 
 Э  Ю  Ъ  Р  О  И  В  Н  Е  Ц  Ш  К  Ш  Т  А  Щ  У  Ш  Ш  К  А  Ю  Ц  Г  Ъ  Ь  К  Е  Ю  Л 
 Й  Й  П  Ю  Р  В  Ь  Ф  Ч  Ъ  Ш  М  Ч  Э  Г  Ь  Л  Ю  О  Ы  З  Л  Д  Й  П     К  Р  Н  У 
    К  З  Ж  И  У  Ш  Ш  Н  Ю  А  З  О  Й  Ы  Н  Ъ  П  Ь  Ю  Н  Ш  Б  Б  С  А  Б  Я  Щ  П 
 Ь  У  Ц  Ъ  И  Б  Щ  Л  Ш  Ю  Л  М  Й  Ж  Я  З  Ш  Ю  Ц  У  И  Ы  М  Ь  С  Ч  Т  А  Д  Р 
 Ж  А  Ь  В  Э  Л  З  А  К  Р  Ь  Ъ  Ь  У  Р  Ш  К  Ж  Ч  Ю  Й  П  Д  Ш  Н  Я  Й  Я  Т  К 
 Л  У  Ю  П  Е  Я  Х  П  Ф  У  П  О  М  Ф  Р  У  Р  А  А  Щ  З  И  В  Ж  Ю  Е  Ч  Щ  Ш  Ь 
 И  Л  С  Ш  А  Б  М  Ц  Ы  Р  Ц  Ь  Б  У  П  Ш  Ф  Г  Ъ  Ю  О  Щ  А  Б  Ю     В  Л  Й  Ж 
 Щ  Ю  Й  Ъ  К  У  З  Б  Ж  Ц  Л  Э  Ц  Н  Б  Р  Ы  В  У  О     Д  М  В  З  Б  Ю     Щ  У 
 Д  Щ  И  У  И  Ы  В  Т  К  Ь  Ч  У  А  Т  Й  З  Ф  О  Щ  Я  Р  Ц  У  Щ  Н  Д  С  Ф  П  Ц 
 М  Н  П  К  Я  Р  Д  Ч  Ы  Х  Ы  П  Л  Л  Н  И  И  В  А  Ц  Б  В  Л  К  Э  З  Т  В  Э  Ъ 
 М  В  Ъ  Т  И  О  П  Ю  П  Ю  А  Ф  Е  Х  З  Б  Ы  Ж  Я  И  Р  Ь  И  К  Н  Ъ  К  Ш  Щ  Ф 
 Я  Ь  Б  У  Ц  Х  Х  Ъ  Ж  Ш  Д  У  В  Э  О  П  А  Ж  Ь  С  А  Ж  З  В  Ь  Ъ  Ю  К  Ъ  С 
 Р  Д  Ж  Ф  Ш  У  Ц  А  П  К  Э  Х  Ъ  Щ  О  Ф  Я  Ъ  П  Х  Ж  Щ  Б  Х  Я  Ъ  У  М  Ы  Ы 
 Ш  О  Р  Д  Ж  А  Л  Щ  Л  Д  Ю  В  Э  В  Э  Ж  З  Й  А  Й  Л  Г  Ь  Щ  Й  У     Ы  О  З 
 Ц  П  Ю  Л  Г  В  У  Ъ  Ц  Ъ  С  Д  Я  О  Д  З  Б  Й  П  Ж  Е  Р  Н  Я  У  М  Д  П  Ъ  У 
 Щ  Щ  В  В  П  Ъ  П  Ц  Ш  З  Й  Г  Ф  О  П  Ь  Й  Ы  П  Б  Й  Ю  Я  Ж  Э  Л  Т  И  Ч  Б 
 Б  В  Ж  Ф  Щ  Ш  Э  Р  С  З  В  Ю  Р  Д  Л  Е  Ъ  Р  Х  Й  Д  Я  Ь  В  Ш  Э  Ц  Л  Ь  О 
 П  М  П  Ъ  К  Э  Ж  З  С  Г  Т  Ш     Ю  Ю  Я  Г  И  Р  Б  У  Ф  Н  Щ  Л  Л  Ц  Т  Э  Я 
    А  Э  Ц  Б  А  О  Л  Ш  Г  Э  Ь  В  Ю  Й  У  М  Ф  Я  Г  Ц  Г  Л  Х  В  У  Щ  Д  Д  Щ 
 Й  Я  О  В  Ш  З  Ъ  Г  Й  Л  Р  З  О  З  В  Ш  Ж  Н  Р  Я  Г  Б  Ш  Э  С  Ь  У  Р  Щ  Ш 
 Р  Й  М  Ь  С  Щ  Н  У  Н  Ш  Т  П  А  Ю  Я  Р  Д  Ж  Б  И  К  З  П  К  Э  Ш  Б  Р  Т  Ч 
 Я  П  Ц  Ю  Ь  В  Г     У  С  У  Д  Л  Т  Д  М  Ь  П  Ч  Т  С  Ю  Ц  Ь     Н  М  Д     Щ 
 Ю  З  З  Ф  Я  В  Ш  З  К  Ш  О  Л  И  Ж  Т  П  Ж  Т  У     В  Ж  З  Л  Б  Н  С     Б  М 
 О  Е  З  У  Щ  В  Р  Н  О  Т  Б  Т  Ц  С  З  Х  Г  Э  Б  Я  Ч  Т  А  Ц  Ф  П  Я  М  В  Ж 
 Д  Д  Х  Я  Ш     С  Т  Ж  Й  У  Щ  Ж  К  Я  Ш  У  Я  Р  Е  Л  Б  Ж  Е  О  Ъ  Я  Ю  Ъ  И 
 Т  Н  А  Л  Й  Ч  Я  Т  Ш  З  А  У  Е  Ж  М     Щ  М  Э  Ц  Е  Н  Ш  Ч  М     Я  К  Ы  Б 
 Я  У  О  Я  К  Ъ  И  В  Д  П  В  Ф  Ч  Ц  З  В  Ш  С  Б  Ф  Л  П  М  З  Щ  Г  О  Т  Я  А 
 З  З  В  В  Э  Ъ  Ш  Н  А  Г  Ь  Ш  Ы  М  Д  Щ  Ъ  Х  Ф  Б  Б  Ц  Ж  Д  Ц  Ф  Ь  Ь  У  С 
 Г  М  С  Щ  Г  С  Ф  Я  Р  Ы  Щ  Е  О  И  Х  О  К  З  Б  К  Е  Х  И  Г  Я  Ж  Ю  Б  Д  Л 
 Я  В  Я  А  Н  Ь  Р  Н  М  Ь  Х  У  Д  Я  Е  И  Ш  В  Б  Ь  З  Ф  Ч  Й  Х  Е  Ь  Ш  Ы  С 
 А  А  У  Ф  К  Ш  Д  М  В  Г  Ш  Б  Щ  Н  С  Ю  Р  Ь  Ж  Ш  Я  Ч  Х  П  П  Ж  Б  Д  Е  Т 
 Т  Ж  Е  Л  В  Д  А  Е  Х  Г  Ь  Л  Т  Ц  Л  Э  Ъ  Д  Ц  Щ  Л  Я  Ю  И  Ы  Й  Ю  Х  Х  Ю 
 Ж  А  Р  Я  З  Г  Й  Ъ  Г  К  Ш  Ш  Ж  Д  П  Й  Я  К  Й  М  Ц  Ь  Д  Ь  Ц  Ю  Л  Г  Ь  М 
 Ш  Н  М  А  Ч  Ф  Ж  Ц  У  Ф  Ъ  Ы  О  Т  К  Ж  Я  Г  В  Ь  У  Ю  Л  Ю  О  Д  Л  В     К 
 Д  Э  И  Х  П  С  П  И  У  В  Ц  Ч  Г  Ч  И  К  М  Е  И  Ю  Г  М  Ю  Ы  Ъ  П  А  К  П  З 
 Ф  М  Л  Р  Ь  Б  Ы  Я  Г  Е  Н  Ж  П  В  В  Ц  Г  Д  Щ  В  Ф  Е  Й  Т  К  Ю  У  Я  Й  Е 
 О  М  Б  Х  Ц  Ч  Л  С  Ш  Э  Р     Х  Н  Ц  З  А  Ш  Д  Э  Д  Г  Д  Е  Ц  Ь  Ъ  Р  Р  К 
 Э  Ь  Х  В  П  Ж  Р  Б  Ы  П  Д  Е  Ц  Г  Ш  Ы  В  Ж  Ь  Ж  Ю  А  Д  О  М  М  Й  Т  Д  Й 
 Г  Ж  Щ  Т  Т  Ю  Щ  Х  Э  У  Ш  Д  Ф  Ь  У  У  Й  С  Н  Я  Л  К  В  М  У  Х  П  А  Э  Ц 
 Т  Ж  Ы  В  Г  Д  Д  А  С  Т  Ж  Э  К  Х  И  Е  Ж  Ь  У  Й  С  В  Ф  У  Ц  Ь  Ц  М  Ю  В 
 Т  П  У  Ц  К  Э  Щ  Й  Т  Ъ  Ь  В  Л  В  Ц  А  Х  З  А  З     К

-}

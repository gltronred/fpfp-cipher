{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TupleSections #-}

module Main where

import Prelude hiding (lookup)

import Control.Arrow
import Control.Monad
import Control.Monad.Par
import Data.Array as A
import Data.Array (Array)
import Data.Char
import Data.Foldable
import Data.Function
import qualified Data.IntSet as IS
import Data.IntSet (IntSet)
import qualified Data.List as L
import Data.List (genericLength,minimumBy,sortBy,group,sort,zip3,zip4)
import qualified Data.Map.Strict as M
import Data.Map.Strict (Map)
import Data.Maybe
import qualified Data.Sequence as Seq
import Data.Sequence (Seq,ViewL(..),(|>))
import System.Environment
import System.IO
import System.Random
import Text.Printf (printf)



newtype Cipher = Cipher { unCipher :: [Int] } deriving (Eq,Ord)

by90 :: String -> String
by90 s | length s < 90 = s
       | otherwise     = take 90 s ++ '\n' : by90 (drop 90 s)

instance Show Cipher where
  show (Cipher ls) = by90 $ concatMap (\l -> ' ' : printf "%0.2d" l) ls



ic :: Cipher -> Double
ic (Cipher xs) = ic' $ map snd $ freq' xs

ic' :: [Int] -> Double
ic' ns = let
  n = fromIntegral $ sum ns
  in sum (map (\k -> fromIntegral $ k*(k-1)) ns) / (n*(n-1))

icTable :: Double
icTable = 0.06399771748902149  -- ic' <$> readTable

readTable :: IO [Int]
readTable = map read . concatMap (tail . words) . lines <$> readFile "table.txt"

toFreqTable :: [Int] -> FreqTable Char
toFreqTable = M.fromList . avg . zip alphabet



newtype Table = Table { getTable :: Array Int (Maybe Char) } deriving (Eq,Show)

emptyArray :: Array Int (Maybe Char)
emptyArray = A.array (0,99) (zip [0..99] $ repeat Nothing)

empty :: Table
empty = Table emptyArray

fromAssocs :: [(Int,Maybe Char)] -> Table
fromAssocs = Table . (emptyArray A.//)

fromList :: [Maybe Char] -> Table
fromList = fromAssocs . zip [0..99]

lookup :: Int -> Table -> Maybe Char
lookup k (Table t) = t!k

insert :: Int -> Maybe Char -> Table -> Table
insert k v (Table t) = Table $ t A.// [(k,v)]



alphabet :: [Char]
alphabet = (' ':['а'..'я'])



-- digramScore :: Map (Char,Char) Double -> Cipher -> Double
-- digramScore digram cipher = sum $ map (\((s1,s2),c) -> case M.lookup (toLower s1,toLower s2) digram of
--                                           Nothing -> 0
--                                           Just d  -> abs $ c-d) $ map (second unFreq) $ freq $ tuple $ unCipher cipher
  
digramsTable :: IO (FreqTable (Char,Char))
digramsTable = M.mapKeysWith (+) (yoToE *** yoToE) . M.fromList . avg .
               map ((\ws -> ((\s -> (s!!0,s!!1)) $ ws!!1,read $ ws!!2)) . words) .
               lines <$> readFile "digrams.txt"
  where forbidden = map (first (\s -> (s!!0,s!!1))) $
                    [ ("  ",-infty)
                    , (" ъ",-infty)
                    , (" ь",-infty)
                    , (" ы",-infty)
                    , ("ьъ",-infty)
                    , ("ъь",-infty)
                    , ("ъъ",-infty)
                    , ("ъ ",-infty) ]
        infty = 1000000
        forbid fs rest = [((x,y),-100000) | x<-alphabet, y<-alphabet] ++ fs ++ rest
        yoToE 'ё' = 'е'
        yoToE x = x



newtype Freq = Freq { unFreq :: Double } deriving (Eq,Ord,Read,Show,Num,NFData)

freq' :: (Eq a, Ord a) => [a] -> [(a, Int)]
freq' = map (head &&& length) . group . sort

freq :: (Eq a, Ord a) => [a] -> [(a, Freq)]
freq = avg . freq'

avg :: [(a,Int)] -> [(a,Freq)]
avg xs = let s = fromIntegral $ sum (map snd xs)
         in map (second $ Freq . (/s) . fromIntegral) xs

tuple s = zip s $ tail s

triple s = zip3 s (tail s) (drop 2 s)



replaces :: Table -> Cipher -> Plain
replaces !rs (Cipher !cs) = mapMaybe (replace rs) cs

replace :: Table -> Int -> Maybe Char
replace (Table !rs) !c = rs ! c



work :: (Show b) => (Cipher -> b) -> IO b
work f = readFile "cipher.txt" >>= return . f . Cipher . map read . concatMap words . lines



-- main = do
--   text <- work id
--   t' <- hillClimbing text 100000 Nothing -- searchEquals 10000000
--   print t'
--   pprint t' text

main = do
  args <- map read <$> getArgs
  let count = fromMaybe 10000000 $ listToMaybe args
  text <- work id
  t <- hillClimbingA count
  print t
  pprint t text




initialTable :: [(Int, Freq)] -> Table
initialTable freqs = fromAssocs $ zip (map fst $ sortBy (compare`on`snd) freqs) (map Just (" сеновалитр" ++ " сеновалитр" ++ (alphabet L.\\ " сеновалитр")) ++ map Just alphabet ++ map Just alphabet ++ map Just alphabet)



type Plain = String

pprint :: Table -> Cipher -> IO ()
pprint t (Cipher cs) = putStrLn $ by90 $ map (fromMaybe '_' . replace t) cs

pprint' :: Table -> Cipher -> IO ()
pprint' t (Cipher cs) = putStrLn $ by90 $ concatMap showMaybe cs
  where showMaybe :: Int -> String
        showMaybe c = maybe (printf " %0.2d" c) (\s -> [' ',toUpper s,' ']) $ replace t c



type FreqTable a = Map a Freq

letters :: (Ord a) => [a] -> FreqTable a
letters = M.fromList . freq

bigrams :: (Ord a) => [a] -> FreqTable (a,a)
bigrams = M.fromList . freq . tuple

computeFreqTable :: (Ord a, Ord b) => (a -> Maybe b) -> FreqTable a -> FreqTable b
computeFreqTable convert as = let
  convert' (a,f) = (,f) <$> convert a
  bfs = mapMaybe convert' $ M.toList as
  in M.fromListWith (+) bfs



score :: (Ord a) => FreqTable a -> FreqTable a -> Double
score bs as = M.foldlWithKey' (\ !s b f -> case M.lookup b as of
                                  Nothing -> s + unFreq f
                                  Just f' -> s + abs (unFreq f - unFreq f')) 0 bs
                                   

changeLetters :: Int -> Int -> Table -> Table
changeLetters i j (Table !t) = let
  li = t ! i
  lj = t ! j
  in Table $! t A.// [(j,li),(i,lj)]

replaceLetter :: Int -> Maybe Char -> Table -> Table
replaceLetter i mc (Table !t) = Table $! t A.// [(i,mc)]

changeTable :: Table -> IO Table
changeTable table@(Table t) = do
  k <- randomRIO (0,9) :: IO Int
  i <- randomRIO $ A.bounds t
  if k == 0
    then (\c -> replaceLetter i (if c==33 then Nothing else Just $ alphabet!!c) table) <$> randomRIO (0,33)
    else (\j -> changeLetters i (if i==j then (j+1)`mod`(snd $ A.bounds t) else j) table) <$> randomRIO (A.bounds t)

hillClimbing :: Cipher -> Int -> Maybe Table -> IO Table
hillClimbing text k mt = do
  ft <-  letters <$> readFile "test.txt" -- toFreqTable <$> readTable
  ftt <- bigrams <$> readFile "test.txt"  -- digramsTable
  let table = fromMaybe (initialTable $ freq $ unCipher text) mt
      lc = letters $ unCipher text
      lc2= bigrams $ unCipher text
  hillClimbing' ft ftt table lc lc2 k



on2 :: (a -> Maybe b) -> (a,a) -> Maybe (b,b)
on2 f (!a1,!a2) = case (f a1, f a2) of
  (Just b1, Just b2) -> Just $! (b1,b2)
  _ -> Nothing



hillClimbing' :: FreqTable Char -> FreqTable (Char,Char) -> Table -> FreqTable Int -> FreqTable (Int,Int) -> Int -> IO Table
hillClimbing' !fp !fp2 !t _ _ 0 = return t
hillClimbing' !fp !fp2 !t !fc !fc2 k = do
  t' <- changeTable t
  let 
    freq  = computeFreqTable (replace t ) fc
    freq' = computeFreqTable (replace t') fc
    fr2   = computeFreqTable (on2 $ replace t ) fc2
    fr2'  = computeFreqTable (on2 $ replace t') fc2
    s freq fr2 = score fp freq -- + score fp2 fr2
    next = if s freq fr2 > s freq' fr2'
           then t'
           else t
  when (k`mod`100 == 0) $ putStrLn $ show k ++ ": " ++ show (s freq fr2) ++ "\t" ++ show (s freq' fr2')
  hillClimbing' fp fp2 next fc fc2 $ k-1


type Nulls = Array Int Bool

changeNulls :: Nulls -> IO Nulls
changeNulls nulls = do
  i <- randomRIO (0,99)
  return $ nulls // [(i, not $ nulls ! i)]

searchNull :: Cipher -> Int -> IO Nulls
searchNull (Cipher !text) k = do
  let counts = A.array (0,99) $ [(x,0) | x<-[0..99]] ++ freq' text
      nulls  = A.array (0,99) $ zip [0..99] $ repeat True
  searchNull' nulls counts k

searchNull' :: Nulls -> Array Int Int -> Int -> IO Nulls
searchNull' nulls _ 0 = return nulls
searchNull' nulls counts k = do
  nulls' <- changeNulls nulls
  let letters ns = foldl' (\l (n,c) -> if n then c:l else l) [] $ zip (toList ns) (toList counts)
      s ns = abs (icTable - ic' (letters ns)) -- + if length (letters ns) < 50 then (50 - genericLength (letters ns))/5 else 0
      next = if (s nulls - s nulls') < 0.01 * s nulls
             then nulls'
             else nulls
  when (k`mod`1000 == 0) $ putStrLn $ show k ++ ":\t" ++ show (s nulls) ++ "\t->\t" ++ show (ic' $ letters nulls) ++ "\t" ++ show (length $ map fst $ filter snd $ assocs nulls)
  searchNull' next counts $ k-1



fixNulls :: Nulls -> Cipher -> Cipher
fixNulls nulls (Cipher cs) = let
  remaining = IS.fromList $ map fst $ filter snd $ assocs nulls
  in Cipher $! filter (flip IS.member remaining) cs



searchSolution :: Int -> Int -> IO (Nulls, Table)
searchSolution k1 k2 = do
  text <- work id
  putStrLn "Search null letters"
  nulls <- searchNull text k1
  let fixed = fixNulls nulls text
  putStrLn "Search substitution"
  t <- hillClimbing fixed k2 Nothing
  print t
  pprint t fixed
  return (nulls, t)



searchEquals :: Int -> IO (Table,Cipher)
searchEquals k = do
  Cipher text <- work id
  letterFreq <- map ((\ws -> (if (ws!!0)!!0==':' then ' ' else (ws!!0)!!0, read $ ws!!1)) . words) . lines <$> readFile "table.txt" :: IO [(Char,Int)]
  let grouped = L.groupBy ((==)`on`snd) $ sortBy (compare`on`snd) $ freq' text
      groupLetters = map (fst &&& map fst . snd) $ zip [0..] grouped
      !cipher = Cipher $ map (\c -> fst $ head $ filter (elem c . snd) groupLetters) text
      alph = map (Just . fst) $ sortBy (compare`on`snd) letterFreq
      initial = Table $ A.array (0,length groupLetters-1) $ zip (map fst groupLetters) (alph ++ repeat Nothing)
  t <- hillClimbing cipher k $ Just initial
  print t
  pprint t cipher
  return (t, cipher)

readAlphabet = zip alphabet <$> readTable

getDistribution alph = let
  s = fromIntegral $ sum $ map snd alph
  to1 0 = 1
  to1 x = x
  in map (second $ to1 . truncate . (*100) . (/s) . fromIntegral) alph

getInitial distr = let
  nullCount = 100 - sum (map snd distr)
  in concatMap (\(c,x) -> replicate x (Just c)) distr ++ replicate nullCount Nothing



changeTableA :: Table -> IO Table
changeTableA table@(Table !t) = do
  i <- randomRIO (0,99)
  j <- randomRIO (0,99)
  return $! if i/=j
            then changeLetters i j table
            else if j==0
                 then replaceLetter i Nothing table
                 else replaceLetter i (Just $ alphabet !! (j`mod`33)) table

randomizeLower :: Table -> IO Table
randomizeLower (Table !t) = do
  let freqs = filter (maybe False (`elem`" еоаи") . snd) $ A.assocs t
  randomTable <- forM [0..99] $ \i -> do
    c <- randomRIO (0,33)
    return (i, if c==33 then Nothing else Just $ alphabet!!c)
  return $! fromAssocs $ randomTable ++ freqs

normalize :: FreqTable a -> FreqTable a
normalize ft = let
  sum = M.foldl' (\sum x -> sum + abs (unFreq x)) 0 ft
  in M.map (Freq . (/sum) . unFreq) ft

readInitial :: Cipher -> IO Table
readInitial (Cipher !text) = do
  alph <- readAlphabet
  let freqCipher = map fst $ sortBy (compare`on`snd) $ freq' text
      table = fromAssocs $! zip freqCipher $ getInitial $ getDistribution alph
  return table

hillClimbingA :: Int -> IO Table
hillClimbingA k = do
  text <- work id
  ft <- letters <$> readFile "test.txt" -- toFreqTable <$> readTable
  ftt <- bigrams <$> readFile "test.txt"
  table <- readInitial text
  let lc = letters $ unCipher text
      lcc = bigrams $ unCipher text
  hillClimbing'A text ft ftt table lc lcc 1.0 0 k

scoreWords t cipher = let
  text = replaces t cipher
  ws = words text
  average = uncurry (/) . foldl' (\(!s,!n) x -> (s+fromIntegral x,n+1)) (0,0)
  good = ["в","на","из","клад"]
  in - genericLength (filter (`elem`good) ws) / 10 + genericLength (filter (==(' ',' ')) $ tuple text) + abs (7 - (average $ map length ws))

digramScore t cipher fpp = let
  fcc = bigrams $ replaces t cipher
  in score fpp fcc

scoreForbid !fr2 = sum $ map (\k -> case M.lookup k fr2 of
                                 Nothing -> 0
                                 Just f  -> 20 * unFreq f) wrongPairs
  where !wrongPairs = [(' ',' ')] ++ [(x,'й') | x<-"бгджзйклмнпрстфхцчшщъь"] ++ [(x,y) | x<-" ьъ", y<-"ъь"]

hillClimbing'A :: Cipher -> FreqTable Char -> FreqTable (Char,Char) -> Table -> FreqTable Int -> FreqTable (Int,Int) -> Double -> Int -> Int -> IO Table
hillClimbing'A _ _ _ !t _ _ _ _ 0 = return t
hillClimbing'A !cipher !fp fpp !t !fc !fcc minScore eq k = do
  t' <- changeTable t
  let s t freq fr2 = score fp freq + score fpp fr2 + scoreForbid fr2
      freq  = computeFreqTable (replace t ) fc
      freq' = computeFreqTable (replace t') fc
      (fr2,fr2')  = runPar $ do
        cfr  <- spawnP $ computeFreqTable (on2 $ replace t ) fcc
        cfr2 <- spawnP $ computeFreqTable (on2 $ replace t') fcc
        fr  <- get cfr
        fr2 <- get cfr2
        return (fr,fr2)
      sc = s t freq fr2
      sc'= s t' freq' fr2'
      next' = if sc > sc'
              then t'
              else t
      eq' = if sc <= sc' then eq+1 else 0
  (minScore',next) <- if eq' == 5000 && sc > 0.01
          then do
            when (sc < 0.7 && sc < minScore || sc < 0.2) $ appendFile "solutions.txt" $ show sc ++ "\n" ++ show t ++ "\n\n"
            (min sc minScore,) <$> if k`mod`20 == 0
                                   then readInitial cipher
                                   else randomizeLower next'
          else return (minScore, next')
  when (k`mod`200 == 0) $ putStrLn $ show k ++ ": " ++ show sc ++ "\t" ++ show sc' ++ "\t" ++ show minScore ++ "\t" ++ show eq' ++ "\t" ++ show (scoreForbid fr2)
  hillClimbing'A cipher fp fpp next fc fcc minScore' eq' $ k-1


{-
Table {getTable = array (0,99) [(0,Nothing),(1,Just '\1078'),(2,Nothing),(3,Just '\1096'),(4,Nothing),(5,Nothing),(6,Just '\1103'),(7,Just '\1089'),(8,Just '\1082'),(9,Just '\1080'),(10,Nothing),(11,Just '\1101'),(12,Nothing),(13,Nothing),(14,Nothing),(15,Nothing),(16,Just '\1102'),(17,Just ' '),(18,Just '\1083'),(19,Just '\1088'),(20,Just '\1081'),(21,Just '\1090'),(22,Just '\1085'),(23,Just '\1092'),(24,Nothing),(25,Just '\1087'),(26,Just '\1100'),(27,Nothing),(28,Nothing),(29,Just '\1088'),(30,Nothing),(31,Nothing),(32,Just '\1083'),(33,Nothing),(34,Just '\1087'),(35,Just '\1080'),(36,Nothing),(37,Just ' '),(38,Just ' '),(39,Just '\1074'),(40,Nothing),(41,Just '\1075'),(42,Nothing),(43,Nothing),(44,Nothing),(45,Just ' '),(46,Nothing),(47,Just '\1072'),(48,Nothing),(49,Nothing),(50,Nothing),(51,Nothing),(52,Nothing),(53,Just '\1099'),(54,Just '\1085'),(55,Nothing),(56,Just '\1076'),(57,Just '\1091'),(58,Nothing),(59,Just ' '),(60,Nothing),(61,Nothing),(62,Nothing),(63,Just '\1086'),(64,Nothing),(65,Just '\1077'),(66,Nothing),(67,Just '\1074'),(68,Just '\1072'),(69,Nothing),(70,Just '\1072'),(71,Nothing),(72,Just '\1077'),(73,Nothing),(74,Nothing),(75,Nothing),(76,Just '\1073'),(77,Nothing),(78,Just '\1072'),(79,Just '\1084'),(80,Nothing),(81,Nothing),(82,Just '\1091'),(83,Nothing),(84,Nothing),(85,Nothing),(86,Nothing),(87,Nothing),(88,Nothing),(89,Just '\1077'),(90,Nothing),(91,Just '\1089'),(92,Nothing),(93,Just '\1079'),(94,Just '\1086'),(95,Just '\1095'),(96,Just '\1086'),(97,Just '\1090'),(98,Nothing),(99,Nothing)]}
-}

{-
Table {getTable = array (0,99) [(0,Just '\1077'),(1,Just '\1085'),(2,Just '\1085'),(3,Just '\1073'),(4,Just '\1088'),(5,Just '\1083'),(6,Just '\1085'),(7,Just '\1103'),(8,Just '\1086'),(9,Nothing),(10,Just ' '),(11,Just '\1093'),(12,Just '\1080'),(13,Just '\1074'),(14,Just '\1082'),(15,Just '\1083'),(16,Just '\1080'),(17,Just '\1086'),(18,Just '\1090'),(19,Just '\1077'),(20,Just ' '),(21,Nothing),(22,Just '\1085'),(23,Just '\1087'),(24,Just '\1090'),(25,Just '\1072'),(26,Just '\1086'),(27,Just '\1077'),(28,Just '\1072'),(29,Just ' '),(30,Just '\1077'),(31,Just '\1085'),(32,Just '\1074'),(33,Just '\1084'),(34,Just '\1073'),(35,Just '\1080'),(36,Just '\1089'),(37,Just ' '),(38,Just '\1088'),(39,Just '\1080'),(40,Just '\1089'),(41,Just '\1091'),(42,Just ' '),(43,Just ' '),(44,Just '\1079'),(45,Just '\1080'),(46,Just '\1085'),(47,Just '\1089'),(48,Just '\1088'),(49,Just '\1086'),(50,Just '\1086'),(51,Just '\1077'),(52,Just '\1087'),(53,Just '\1079'),(54,Just '\1074'),(55,Just '\1072'),(56,Just ' '),(57,Just '\1077'),(58,Just '\1089'),(59,Just '\1086'),(60,Just '\1072'),(61,Just '\1091'),(62,Just '\1088'),(63,Just '\1076'),(64,Just ' '),(65,Just '\1077'),(66,Just '\1080'),(67,Just '\1090'),(68,Just '\1095'),(69,Nothing),(70,Nothing),(71,Just '\1077'),(72,Just '\1076'),(73,Just ' '),(74,Just ' '),(75,Just '\1072'),(76,Just '\1103'),(77,Nothing),(78,Just '\1089'),(79,Just '\1083'),(80,Just ' '),(81,Just ' '),(82,Just '\1095'),(83,Nothing),(84,Just '\1072'),(85,Just '\1090'),(86,Just '\1083'),(87,Just '\1082'),(88,Just '\1082'),(89,Just '\1090'),(90,Just '\1087'),(91,Just '\1087'),(92,Just '\1084'),(93,Just '\1091'),(94,Just '\1072'),(95,Just '\1074'),(96,Just '\1086'),(97,Just '\1084'),(98,Just '\1086'),(99,Nothing)]}
-}

{-
Table {getTable = array (0,99) [(0,Nothing),(1,Just '\1083'),(2,Just '\1087'),(3,Just '\1079'),(4,Just ' '),(5,Just '\1082'),(6,Just '\1076'),(7,Just '\1086'),(8,Just '\1086'),(9,Just '\1089'),(10,Nothing),(11,Just '\1077'),(12,Just '\1077'),(13,Just '\1074'),(14,Just '\1090'),(15,Just ' '),(16,Nothing),(17,Just '\1089'),(18,Just '\1090'),(19,Just '\1100'),(20,Just '\1091'),(21,Just '\1074'),(22,Just ' '),(23,Just '\1099'),(24,Just ' '),(25,Just '\1072'),(26,Just '\1091'),(27,Just '\1080'),(28,Just '\1072'),(29,Just '\1086'),(30,Just '\1100'),(31,Just '\1082'),(32,Just '\1085'),(33,Just '\1090'),(34,Just '\1089'),(35,Just '\1074'),(36,Just '\1085'),(37,Just '\1077'),(38,Just ' '),(39,Nothing),(40,Just ' '),(41,Just '\1080'),(42,Nothing),(43,Just '\1072'),(44,Just '\1083'),(45,Just '\1072'),(46,Just '\1095'),(47,Just '\1099'),(48,Just ' '),(49,Just '\1080'),(50,Just '\1086'),(51,Just '\1086'),(52,Nothing),(53,Just '\1087'),(54,Just '\1084'),(55,Just '\1080'),(56,Nothing),(57,Just '\1080'),(58,Just '\1099'),(59,Just '\1072'),(60,Nothing),(61,Just '\1077'),(62,Just '\1083'),(63,Just ' '),(64,Just '\1085'),(65,Just '\1080'),(66,Just '\1103'),(67,Just '\1074'),(68,Just ' '),(69,Just '\1088'),(70,Just '\1084'),(71,Just '\1077'),(72,Just '\1087'),(73,Just '\1079'),(74,Nothing),(75,Nothing),(76,Just '\1076'),(77,Just '\1083'),(78,Just '\1089'),(79,Just '\1090'),(80,Just '\1095'),(81,Just '\1086'),(82,Just '\1083'),(83,Just '\1088'),(84,Just '\1086'),(85,Just '\1088'),(86,Just '\1085'),(87,Just '\1076'),(88,Just ' '),(89,Just '\1082'),(90,Just '\1091'),(91,Just '\1103'),(92,Just ' '),(93,Just '\1080'),(94,Nothing),(95,Just '\1096'),(96,Just '\1086'),(97,Just ' '),(98,Just '\1077'),(99,Just '\1085')]}
-}

{-
0.34

Table {getTable = array (0,99) [(0,Nothing),(1,Just '\1072'),(2,Just '\1080'),(3,Just '\1082'),(4,Just '\1072'),(5,Just '\1086'),(6,Just ' '),(7,Just '\1073'),(8,Just '\1076'),(9,Nothing),(10,Just '\1085'),(11,Just '\1092'),(12,Just ' '),(13,Just '\1072'),(14,Just '\1080'),(15,Just '\1086'),(16,Just ' '),(17,Just '\1075'),(18,Just '\1086'),(19,Just '\1083'),(20,Just '\1075'),(21,Just ' '),(22,Just '\1077'),(23,Just '\1089'),(24,Just '\1086'),(25,Just '\1095'),(26,Just ' '),(27,Just '\1082'),(28,Just '\1090'),(29,Just '\1095'),(30,Just '\1074'),(31,Just '\1072'),(32,Just '\1072'),(33,Just '\1099'),(34,Just '\1080'),(35,Just '\1090'),(36,Just ' '),(37,Just '\1072'),(38,Just ' '),(39,Just '\1076'),(40,Just '\1100'),(41,Just '\1073'),(42,Just '\1089'),(43,Just '\1087'),(44,Just '\1086'),(45,Nothing),(46,Just '\1077'),(47,Just '\1090'),(48,Just '\1080'),(49,Just '\1082'),(50,Nothing),(51,Just '\1085'),(52,Just '\1087'),(53,Just '\1099'),(54,Just '\1080'),(55,Just '\1090'),(56,Just '\1088'),(57,Just '\1083'),(58,Just '\1084'),(59,Just '\1084'),(60,Just '\1088'),(61,Just '\1087'),(62,Just '\1086'),(63,Just ' '),(64,Just ' '),(65,Just '\1074'),(66,Just '\1088'),(67,Nothing),(68,Just '\1077'),(69,Just ' '),(70,Nothing),(71,Just '\1085'),(72,Just '\1091'),(73,Just '\1082'),(74,Just '\1077'),(75,Just '\1088'),(76,Just '\1079'),(77,Just ' '),(78,Just '\1090'),(79,Just '\1086'),(80,Just ' '),(81,Just '\1074'),(82,Just '\1100'),(83,Just ' '),(84,Just '\1093'),(85,Just '\1091'),(86,Just ' '),(87,Just ' '),(88,Just '\1077'),(89,Just '\1086'),(90,Just '\1074'),(91,Just '\1085'),(92,Just '\1080'),(93,Just '\1079'),(94,Just '\1083'),(95,Just '\1091'),(96,Just '\1089'),(97,Just '\1077'),(98,Just '\1083'),(99,Just '\1089')]}
-}

{-
0.34

Table {getTable = array (0,99) [(0,Nothing),(1,Just ' '),(2,Just '\1080'),(3,Just '\1084'),(4,Just '\1080'),(5,Just '\1086'),(6,Just '\1083'),(7,Just '\1090'),(8,Nothing),(9,Just '\1083'),(10,Just '\1082'),(11,Just '\1080'),(12,Nothing),(13,Just '\1077'),(14,Just '\1080'),(15,Just '\1072'),(16,Just '\1088'),(17,Just '\1088'),(18,Just '\1072'),(19,Just '\1088'),(20,Just '\1074'),(21,Just ' '),(22,Just '\1072'),(23,Just '\1089'),(24,Just '\1086'),(25,Just '\1096'),(26,Just '\1085'),(27,Just '\1082'),(28,Just '\1082'),(29,Just '\1089'),(30,Just '\1076'),(31,Just '\1099'),(32,Just '\1077'),(33,Just '\1072'),(34,Just '\1077'),(35,Just '\1090'),(36,Just ' '),(37,Just '\1103'),(38,Just '\1086'),(39,Just '\1085'),(40,Just '\1086'),(41,Just '\1093'),(42,Just '\1079'),(43,Just '\1085'),(44,Just '\1072'),(45,Nothing),(46,Nothing),(47,Just '\1075'),(48,Nothing),(49,Just '\1074'),(50,Just '\1085'),(51,Just '\1084'),(52,Just '\1076'),(53,Just '\1091'),(54,Just '\1077'),(55,Just '\1085'),(56,Just '\1089'),(57,Just '\1083'),(58,Just '\1085'),(59,Just '\1074'),(60,Just '\1083'),(61,Just '\1087'),(62,Just '\1086'),(63,Just '\1086'),(64,Just ' '),(65,Just '\1084'),(66,Just '\1088'),(67,Just ' '),(68,Just '\1077'),(69,Just ' '),(70,Just ' '),(71,Just '\1087'),(72,Just '\1080'),(73,Just ' '),(74,Just '\1080'),(75,Just '\1076'),(76,Just ' '),(77,Just ' '),(78,Just '\1085'),(79,Just '\1077'),(80,Just '\1089'),(81,Just '\1090'),(82,Just '\1099'),(83,Just ' '),(84,Just '\1077'),(85,Just '\1091'),(86,Nothing),(87,Just ' '),(88,Just '\1086'),(89,Just '\1091'),(90,Just '\1089'),(91,Just '\1090'),(92,Nothing),(93,Just '\1079'),(94,Nothing),(95,Just '\1072'),(96,Just '\1095'),(97,Just ' '),(98,Just '\1075'),(99,Just ' ')]}
-}

{-
0.005

Table {getTable = array (0,99) [(0,Just '\1082'),(1,Just ' '),(2,Just '\1090'),(3,Just '\1084'),(4,Just '\1090'),(5,Just '\1085'),(6,Just '\1085'),(7,Just '\1089'),(8,Just '\1100'),(9,Just '\1077'),(10,Just '\1072'),(11,Just '\1098'),(12,Just '\1072'),(13,Just ' '),(14,Just ' '),(15,Just '\1091'),(16,Just '\1086'),(17,Just '\1077'),(18,Just '\1080'),(19,Just '\1085'),(20,Just ' '),(21,Just '\1091'),(22,Just '\1073'),(23,Just '\1097'),(24,Just ' '),(25,Just '\1080'),(26,Just ' '),(27,Just '\1080'),(28,Just '\1090'),(29,Just '\1087'),(30,Just '\1078'),(31,Just '\1086'),(32,Just '\1082'),(33,Just '\1088'),(34,Just '\1077'),(35,Just ' '),(36,Just '\1077'),(37,Nothing),(38,Just '\1099'),(39,Just '\1086'),(40,Just '\1084'),(41,Just ' '),(42,Just ' '),(43,Just '\1076'),(44,Just ' '),(45,Just '\1083'),(46,Just '\1077'),(47,Just '\1072'),(48,Just '\1089'),(49,Just '\1072'),(50,Just '\1075'),(51,Just ' '),(52,Just '\1088'),(53,Just '\1092'),(54,Just '\1089'),(55,Just '\1096'),(56,Just '\1086'),(57,Just '\1086'),(58,Just '\1072'),(59,Just ' '),(60,Just '\1088'),(61,Just '\1072'),(62,Just ' '),(63,Just '\1080'),(64,Just '\1088'),(65,Just '\1083'),(66,Just '\1077'),(67,Just '\1083'),(68,Just '\1080'),(69,Just '\1074'),(70,Just '\1085'),(71,Just '\1086'),(72,Just '\1075'),(73,Just '\1080'),(74,Just ' '),(75,Just '\1086'),(76,Just '\1093'),(77,Just '\1090'),(78,Just '\1087'),(79,Just '\1083'),(80,Just '\1074'),(81,Nothing),(82,Just '\1102'),(83,Just '\1095'),(84,Just '\1084'),(85,Just '\1074'),(86,Just '\1086'),(87,Just '\1081'),(88,Just '\1087'),(89,Just '\1074'),(90,Just '\1086'),(91,Just '\1085'),(92,Just '\1082'),(93,Just '\1086'),(94,Just '\1072'),(95,Just ' '),(96,Just '\1103'),(97,Just '\1089'),(98,Just '\1079'),(99,Just '\1076')]}
-}
